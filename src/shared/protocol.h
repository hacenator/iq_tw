#pragma once

#include "common.h"
#include "message.h"

namespace fsh
{

// forward
class Peer;

enum class EProtocolResult : uint16_t
{
    COMPLETE,
    PASS
};

class IProtocol
{
public:
    virtual ~IProtocol()
    {
    }
    
    virtual EProtocolResult processMessage(const PeerID& peer, const Message* msg) = 0;
};

class Protocol 
    : public IProtocol
{
public:
    explicit Protocol(Peer* handler) 
        : _handler(handler)
    {

    }

    virtual ~Protocol()
    {
        
    }
    
protected:
    Peer* _handler;
};

}
