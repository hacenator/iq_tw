#pragma once

#include "common.h"
#include "message.h"

namespace fsh
{

/*
    Join request
*/
class MessageJoinRequest final
	: public Message
{
public:
    MessageJoinRequest()
		: Message(EMessageType::NET_MESSAGE_JOINREQUEST)
        , _versionCode(0)
	{
	}

	void setNickname(const NetString& value)
	{
		_nickname = value;
	}

	const NetString& getNickname() const
	{
		return _nickname;
	}

	void visit(IVisitor* visitor) override
	{
		visitor->accept(_nickname);
		visitor->accept(_versionCode);
	}

    uint32_t getVersionCode() const
    {
        return _versionCode;
    }

    void setVersionCode(uint32_t value)
    {
        _versionCode = value;
    }

protected:
    uint32_t _versionCode;
	NetString _nickname;
};

/*
    Join response
*/
class MessageJoinResponse final
    : public Message
{
public:
    enum class EJoinResult : uint16_t
    {
        Accept,                     // accept client
        Failed_UserAlreadyExist,    // user with the same nickname already exists
        Failed_Version,             // unsupported version
        Failed_Other,               // unknown reason
    };

    MessageJoinResponse()
        : Message(EMessageType::NET_MESSAGE_JOINRESPONSE)
    {
    }
    
    void setResult(EJoinResult value)
    {
        _result = value;
    }

    EJoinResult getResult() const
    {
        return _result;
    }

    void visit(IVisitor* visitor) override
    {
        visitor->accept(_result);
    }

protected:
    EJoinResult _result;
};

}
