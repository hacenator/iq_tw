#include "socket.h"

#ifdef F_IPV6
#define AF_DEFAULT AF_INET6
#define SOCKADDR_SIZE sizeof(sockaddr_in6)
#else
#define AF_DEFAULT AF_INET
#define SOCKADDR_SIZE sizeof(sockaddr_in)
#endif

fsh::Socket::Socket(size_t buffersSize /*= 1024*/)
    : _socket(INVALID_SOCKET)
    , _state(ESocketState::Closed)
    , _buffersSize(buffersSize)
    , _incomingBuffer(nullptr)
    , _outcomingBuffer(nullptr)
    , _onReceived(nullptr)
    , _toSend(0)
    , _type(ESocketType::Disabled)
{

}

fsh::Socket::~Socket()
{
    close();
}

void fsh::Socket::open()
{
    F_ASSERT(_socket == INVALID_SOCKET, "Socket already opened");

    if (_socket != INVALID_SOCKET)
        close();

    _socket = socket(AF_DEFAULT, SOCK_STREAM, IPPROTO_TCP);

    F_ASSERT(_socket != INVALID_SOCKET, "Failed to create socket");

    if (_socket != INVALID_SOCKET)
        _state = ESocketState::Opened;

    // non blocking socket
#if defined(F_PLATFORM_WINDOWS)
    u_long value = 1;
    int status = ioctlsocket(_socket, FIONBIO, &value);
    F_ASSERT(status == 0, "Failed to set socket to be nonblocking");
#elif defined(F_PLATFORM_MAC)
    int flags;
    flags = fcntl(_socket, F_GETFL);
    F_ASSERT(flag != -1, "Failed to get socket flags");
    fcntl(_socket, F_SETFL, flags | O_NONBLOCK);
    
    // reuse
    const int reuse = 1;
    int status = INVALID_SOCKET;
    status = setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, static_cast<const void*>(&reuse), sizeof(reuse));
    F_ASSERT(status != INVALID_SOCKET, "setsockopt(SO_REUSEADDR) failed");
    
    status = setsockopt(_socket, SOL_SOCKET, SO_REUSEPORT, static_cast<const void*>(&reuse), sizeof(reuse));
    F_ASSERT(status != INVALID_SOCKET, "setsockopt(SO_REUSEPORT) failed");
#endif
}

void fsh::Socket::close()
{
    if (_socket != INVALID_SOCKET)
    {
        int status = 0;
        
#if defined(F_PLATFORM_WINDOWS)
        status = shutdown(_socket, SD_BOTH);
#elif defined(F_PLATFORM_MAC)
        status = shutdown(_socket, SHUT_RDWR);
#endif
        
        F_UNUSED(status);
        F_ASSERT(status == 0, "Failed to shutdown socket");
        
#if defined(F_PLATFORM_WINDOW)
        status = closesocket(_socket);
#elif defined(F_PLATFORM_MAC)
        status = ::close(_socket);
#endif
        
        F_ASSERT(status == 0, "Failed to close socket");
        
        _socket = INVALID_SOCKET;
        _state = ESocketState::Closed;
        
        // Buffers
        if (_incomingBuffer)
        {
            free(_incomingBuffer);
            _incomingBuffer = nullptr;
        }
        
        if (_outcomingBuffer)
        {
            free(_outcomingBuffer);
            _outcomingBuffer = nullptr;
        }
    }
}

fsh::Socket::SockAddr fsh::Socket::getSockAddrFor(const char* addr, uint16_t port)
{
    SockAddr newSockAddr;

#ifdef F_IPV6
    newSockAddr.sa.ipv6.sin6_family = AF_DEFAULT;
    inet_pton(AF_DEFAULT, addr, &newSockAddr.sa.ipv6.sin6_addr);
    newSockAddr.sa.ipv6.sin6_port = htons(port);
#else
    newSockAddr.sa.ipv4.sin_family = AF_DEFAULT;
    inet_pton(AF_DEFAULT, addr, &newSockAddr.sa.ipv4.sin_addr);
    newSockAddr.sa.ipv4.sin_port = htons(port);
#endif

    return newSockAddr;
}

bool fsh::Socket::listen(const char* addr, uint16_t port)
{
    if (_state == ESocketState::Closed)
        open();

    SockAddr sa = getSockAddrFor(addr, port);

    // bind
    int status = ::bind(_socket, reinterpret_cast<sockaddr*>(&sa), SOCKADDR_SIZE);
    F_ASSERT(status != SOCKET_ERROR, "Failed to bind socket");
    if (status != SOCKET_ERROR)
    {
        _state = ESocketState::Binded;
    }
    else
    {
        return false;
    }

    // listen
    status = ::listen(_socket, SOMAXCONN);
    if (status != SOCKET_ERROR)
    {
        _state = ESocketState::Listening;
        _type = ESocketType::Incoming;
        return true;
    }

    return false;
}

bool fsh::Socket::connect(const char* addr, uint16_t port)
{
    if (_state == ESocketState::Closed)
        open();

    SockAddr sa = getSockAddrFor(addr, port);

    initBuffers();
    
    // connect
    int status = ::connect(_socket, reinterpret_cast<sockaddr*>(&sa), SOCKADDR_SIZE);
    if (status == SOCKET_ERROR)
    {
        int error = SOCKET_LASTERROR;
        
        F_ASSERT(error == SOCKET_INPROGRESS, "Failed to connect to the addr");

        if (error == SOCKET_INPROGRESS)
        {
            _state = ESocketState::Connecting;
            _type = ESocketType::Outcoming;
            return true;
        }
        else
        {
            return false;
        }
    }

    if (status != SOCKET_ERROR)
    {
        _state = ESocketState::Idle;
        _type = ESocketType::Outcoming;
        return true;
    }

    return false;
}

bool fsh::Socket::accept(Socket* socket)
{
    sockaddr_storage ss;
    socklen_t sslen = sizeof(ss);
    
    initBuffers();
    
    _socket = ::accept(socket->getHandle(), reinterpret_cast<sockaddr*>(&ss), &sslen);
    
    if (_socket != SOCKET_ERROR)
    {
        _state = ESocketState::Idle;
        _type = ESocketType::Incoming;
        return true;
    }
    
    return false;
}

void fsh::Socket::initBuffers()
{
    // buffers
    if (!_incomingBuffer && _buffersSize)
        _incomingBuffer = malloc(_buffersSize);
    
    if (!_outcomingBuffer && _buffersSize)
        _outcomingBuffer = malloc(_buffersSize);
}

ssize_t fsh::Socket::read()
{
    if (_state == ESocketState::Idle && _incomingBuffer)
    {
        size_t size = _buffersSize;
        ssize_t received = recv(_socket, static_cast<char*>(_incomingBuffer), size, 0);        
       
        if (received != SOCKET_ERROR)
        {
            if (_onReceived)
                _onReceived(_incomingBuffer, received);
        }
        else if (received == 0) // TCP - connection refused
        {
            close();
        }
#if defined(F_DEBUG)
        else
        {
            int error = SOCKET_LASTERROR;
            error;
        }
#endif
            
        return received;
    }
    return SOCKET_ERROR;
}

ssize_t fsh::Socket::write()
{
    if (_state == ESocketState::Idle && _outcomingBuffer)
    {
        size_t size = _toSend;
        ssize_t sent = ::send(_socket, static_cast<const char*>(_outcomingBuffer), size, 0);
        
        if (sent != size)
        {
            F_ASSERT(false, "Ooops");
        }
        
        if (sent != SOCKET_ERROR)
        {
            _toSend = 0;
            _state = ESocketState::Idle;
        }
#ifdef defined(F_DEBUG)
        else
        {
            int error = SOCKET_LASTERROR;
            error;
        }
#endif
        
        return sent;
    }

    return SOCKET_ERROR;
}

bool fsh::Socket::send(const void* buffer, size_t size)
{
    // not ready to send
    //if (_state != ESocketState::Idle)
    //    return false;
    
    // data overflow
    if (size > _buffersSize)
        return false;
    
    // send already queued
    if (_toSend != 0)
        return false;
    
    if (!_outcomingBuffer)
        return false;
    
    _toSend = size;
    memcpy(_outcomingBuffer, buffer, _toSend);
    
    if (_state == ESocketState::Idle)
        write();
    
    return true;
}
