/* 
So this is not a plain Visitor pattern idiology.
Actualy this implementation have a little bit reversed meaning, this is the smart serializatior.
*/

#pragma once

#include <type_traits>
#include <functional>
#include <string>
#include "common.h"
//#include "allocator.h"

namespace fsh
{

// forwarded
class IVisitable;

enum class EVisitorType : uint8_t
{
    Reader = 0, // visitor reads data from the buffer (deserialize)
    Writer = 1, // visitor writes data to the buffer (serialize)
};

class IVisitor
{
public:
    IVisitor(EVisitorType type) 
		: _type(type)
    {
    }

    virtual ~IVisitor() {}

    /* POD processing */
    template<class T>
    typename std::enable_if<std::is_pod<T>::value, void>::type
    accept(const T& value)
    {
        accept(&value, sizeof(T));
    }

    template<class T>
    typename std::enable_if<std::is_pod<T>::value, void>::type
    accept(T& value)
    {
        accept(&value, sizeof(T));
    }

    /* internal classes processing */
    template<class T>
    typename std::enable_if<!std::is_pod<T>::value, void>::type
        accept(const T& value)
    {
        value.visit(this);
    }

    template<class T>
    typename std::enable_if<!std::is_pod<T>::value, void>::type
        accept(T& value)
    {
        value.visit(this);
    }

    /* Default plain types */
	void accept(NetString& string)
	{
        const uint8_t charSize(sizeof(NetString::traits_type::char_type));
        
		if (isReader())
		{
            static NetString::traits_type::char_type buffer[128]; // not thread safe, right
            
			uint32_t size = 0;
			accept(size);

			if (size)
			{
				if (size <= 512)
				{
					accept(buffer, size * charSize);
					string.assign(buffer, size);
				}
				else
				{
					wchar_t* dynamicBuffer = static_cast<wchar_t*>(malloc(size));
					F_ASSERT(dynamicBuffer, "Failed to allocate memory");
					accept(dynamicBuffer, size);
					string.assign(buffer, size);
					free(dynamicBuffer);
				}
			}
		}
		else
		{
			const uint32_t size(checked_numcast<uint32_t>(string.size()));
			accept(size);
			accept(string.data(), size * charSize);
		}
	}

    /* accept */
    virtual void accept(const void* data, size_t size) = 0;
    virtual void accept(void* data, size_t size) = 0;
    
    void acceptConst(const void* data, size_t size)
    {
        accept(data, size);
    }

    void acceptNonConst(void* data, size_t size)
    {
        accept(data, size);
    }

    /*
        True if visitor writes data to the buffer (serialize)
        IVisitable -> buffer
    */
    bool isWriter() const
    {
        return (_type == EVisitorType::Writer);
    }

    /*
        True if visitor reads data from the buffer (deserialize)
        buffer -> IVisitable
    */
    bool isReader() const
    {
        return (_type == EVisitorType::Reader);
    }

private:
    EVisitorType _type;
};

class IVisitable
{
public:
    virtual void visit(IVisitor* visitor) = 0;
};

}
