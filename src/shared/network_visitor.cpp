#include "network_visitor.h"

fsh::NetworkVisitor::NetworkVisitor(size_t size /*= 1024*/)
	: IVisitor(EVisitorType::Writer)
	, _offset(0)
	, _bufferSize(size)
{
	_data = static_cast<char*>(malloc(size));
}

fsh::NetworkVisitor::NetworkVisitor(const void* data, size_t size)
	: IVisitor(EVisitorType::Reader)
	, _offset(0)
	, _bufferSize(size)
{
	_data = static_cast<char*>(malloc(size));
	memcpy(_data, data, size);
}

fsh::NetworkVisitor::~NetworkVisitor()
{
	free(_data);
	_data = nullptr;
}

void fsh::NetworkVisitor::accept(const void* data, size_t size)
{
	F_ASSERT(_offset + size <= _bufferSize, "Data overflow");
	if (_offset + size <= _bufferSize)
	{
		memcpy(_data + _offset, data, size);
		_offset += size;
	}
}

void fsh::NetworkVisitor::accept(void* data, size_t size)
{
	if (isWriter())
	{
		F_ASSERT(_offset + size <= _bufferSize, "Data overflow");
		if (_offset + size <= _bufferSize)
		{
			memcpy(_data + _offset, data, size);
			_offset += size;
		}
	}
	else
	{
		F_ASSERT(_offset + size <= _bufferSize, "Data overflow");
		if (_offset + size <= _bufferSize)
		{
			memcpy(data, _data + _offset, size);
			_offset += size;
		}
	}
}

void fsh::NetworkVisitor::reset()
{
	_offset = 0;
}
