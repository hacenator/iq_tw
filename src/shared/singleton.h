#pragma once

namespace fsh
{

template <class T>
class Singleton
{
public:
    inline static T* getInstance()
    {
        if (!_instance)
            _instance = new T();

        return _instance;
    }

	inline static void shutdown()
	{
		delete _instance;
	}

    inline static bool isInstanced()
    {
        return (_instance != nullptr);
    }

protected:
    static T* _instance;

};

}

//
#include "singleton.inl"
