#pragma once

#include "message.h"
#include <vector>

namespace fsh
{

/*
    
*/
typedef std::vector<Message*> MessagesVector;

/*
    Message factory
*/
class MessageFactory
{
public:
    MessageFactory(uint32_t size = 1024);
    ~MessageFactory();
    
    Message* allocMessage(EMessageType type);
    void freeMessage(Message* msg);
    
private:
    Message* createMessage(EMessageType type);

    MessagesVector _messages;
};
    
}
