#include "peer.h"
#include "network_visitor.h"

// server default config
namespace
{
    const uint16_t sMaxClients = 16;
}

static_assert(sizeof(uint16_t) == sizeof(fsh::EMessageType), "Size mismatch");

// Sockets listener
class SocketsListener
    : public fsh::ISocketsListener
{
public:
    SocketsListener(fsh::Peer* peer)
        : _peer(peer)
    {
    }

    void onIncomingConnection(fsh::Socket* connection) override
    {
        _peer->onClientConnected(connection->getId());
    }

    void onOutcomingConnection(fsh::Socket* connection) override
    {
        _peer->onConnected(connection->getId());
    }

    void onConnectionClosed(fsh::Socket* connection) override
    {
        F_UNUSED(connection);
    }

    // recieve
    void onIncomingData(fsh::Socket* connection) override
    {
        F_UNUSED(connection);
    }

    void onDataReceived(fsh::Socket* connection, const void* data, const size_t size) override
    {
        fsh::MessageFactory* msgFactory(_peer->getMessageFactory());

        fsh::NetworkVisitor netVisitor(data, size);
        fsh::IVisitor* visitor = &netVisitor;

        fsh::EMessageType msgType;
        visitor->accept(msgType);

        F_ASSERT(fsh::checked_numcast<uint16_t>(msgType) < fsh::checked_numcast<uint16_t>(fsh::EMessageType::NET_MESSAGE_MAX), "Unsupported message type");

        fsh::Message* msg = msgFactory->allocMessage(msgType);
        if (msg)
        {
            msg->setSrcPeer(connection->getId());
            msg->visit(visitor);

            _peer->registerIncomingMessage(msg);
        }
    }

    // send
    void onDataSent(fsh::Socket* connection) override
    {
        F_UNUSED(connection);
    }

    void onError(fsh::Socket* connection, fsh::ESocketError reason) override
    {
        F_UNUSED(connection);
        F_UNUSED(reason);
    }

private:
    fsh::Peer* _peer;
};

// Peer
fsh::Peer::Peer()
    : _socketManager(nullptr)
    , _socketsListener(nullptr)
    , _messageFactory(nullptr)
{

}

fsh::Peer::~Peer()
{
    destroy();
}

void fsh::Peer::init()
{
    //
    _socketsListener = new SocketsListener(this);
    _socketManager = new SocketManager();
    _socketManager->init(sMaxClients + 1, _socketsListener);

    _messageFactory = new MessageFactory();
}

void fsh::Peer::destroy()
{
    // protocols
    std::for_each(_protocols.begin(), _protocols.end(), [](IProtocol* protocol)
    {
        delete protocol;
    });
    _protocols.clear();

    // messages
    delete _messageFactory;
    _messageFactory = nullptr;

    // sockets listener
    delete _socketsListener;
    _socketsListener = nullptr;

    // socket manager
    delete _socketManager;
    _socketManager = nullptr;
}

void fsh::Peer::update()
{
    Message* msg = nullptr;

    // process incoming messages
    while (!_queueIncoming.empty())
    {
        msg = _queueIncoming.front();
        bool result = processMessage(msg->getSrcPeer(), msg);
        F_UNUSED(result);
        F_ASSERT(result, "Failed to process message");
        _messageFactory->freeMessage(msg);

        _queueIncoming.pop();
    }

    // process outcoming messages
    NetworkVisitor netVisitor;
    IVisitor* visitor = &netVisitor;

    while (!_queueOutcoming.empty())
    {
        msg = _queueOutcoming.front();
        const PeersList& peers = msg->getDstPeersList();
        const size_t nDstPeers(peers.size());

        netVisitor.reset();

        if (nDstPeers)
        {
            // append EMessageType
            uint16_t value = checked_numcast<uint16_t>(msg->getType());
            visitor->accept(value);
            msg->visit(visitor);

            for (size_t i(0); i < nDstPeers; ++i)
            {
                Socket* peerSocket = _socketManager->getSocket(peers[i]);
                F_ASSERT(peerSocket, "Requested peer not available");
                if (peerSocket)
                {
                    const bool result = peerSocket->send(netVisitor.getData(), netVisitor.getSize());
                    F_UNUSED(result);
                    F_ASSERT(result, "Failed to send message");
                }
            }
        }
        _messageFactory->freeMessage(msg);

        _queueOutcoming.pop();
    }

    // process sockets
    _socketManager->update();
}

void fsh::Peer::registerProtocol(Protocol* protocol)
{
    _protocols.insert(static_cast<IProtocol*>(protocol));
}

void fsh::Peer::registerOutcomingMessage(Message* msg)
{
	if (msg->getDstPeersList().size())
		_queueOutcoming.push(msg);
}

void fsh::Peer::registerIncomingMessage(Message* msg)
{
    _queueIncoming.push(msg);
}

bool fsh::Peer::processMessage(const PeerID& peer, const Message* msg)
{
    ProtocolsSet::iterator it = _protocols.begin();

    bool done = false;
    while (!done && it != _protocols.end())
    {
        switch ((*it)->processMessage(peer, msg))
        {
            case EProtocolResult::PASS:
                break;
            case EProtocolResult::COMPLETE:
                done = true;
                break;
        }

        ++it;
    }

    return done;
}
