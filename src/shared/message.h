#pragma once

#include "common.h"
#include "visitor.h"
#include "socket.h"

#include <vector>

namespace fsh
{

// PeerID 
typedef SocketID PeerID;
typedef std::vector<PeerID> PeersList;

// Available messages
enum class EMessageType : uint16_t
{
	NET_MESSAGE_JOINREQUEST,
    NET_MESSAGE_JOINRESPONSE,
    
    NET_MESSAGE_CHATMESSAGE,

    NET_MESSAGE_MAX,
};

// Generic message
class Message
	: public IVisitable
{
public:
	Message(EMessageType type)
		: _type(type)
	{

	}

	virtual ~Message()
	{

	}

	EMessageType getType() const
	{
		return _type;
	}

    PeerID getSrcPeer() const
    {
        return _srcPeer;
    }

    void setSrcPeer(const PeerID& peer)
    {
        _srcPeer = peer;
    }

    const PeersList& getDstPeersList() const
    {
        return _dstPeers;
    }
    
    void addDstPeer(const PeerID& peer)
    {
        _dstPeers.push_back(peer);
    }

protected:
    const EMessageType _type;

    PeerID _srcPeer;
    PeersList _dstPeers;
};

}
