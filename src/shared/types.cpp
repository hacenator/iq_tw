#include "types.h"

const size_t fsh::Vec2TC::stride = sizeof(fsh::Vec2TC);
const size_t fsh::Vec2C::stride = sizeof(fsh::Vec2C);
