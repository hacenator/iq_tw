#pragma once

#include <stdint.h>
#include <string>
#include "netstring.h"

// configs
#define F_VERSIONCODE 2
//#define F_IPV6

//
#if defined(_WIN32)
    #define F_PLATFORM_WINDOWS
#elif defined(__APPLE__)
    #define F_PLATFORM_MAC
#else
    #error Unsupported platform
#endif

#define OFFSETOF(s,m) ((void*)&reinterpret_cast<char const volatile&>((((s*)0)->m)))

// Windows
#ifdef F_PLATFORM_WINDOWS
    #include <WinSock2.h>
    #include <Ws2tcpip.h>
    //#include <Windows.h>

	typedef long ssize_t;

    #define F_BREAK() __debugbreak()
    #define SOCKET_INPROGRESS WSAEWOULDBLOCK
	#define SOCKET_LASTERROR WSAGetLastError()
    #define SOCKET_REFUSED (0)
#endif

// Mac
#ifdef F_PLATFORM_MAC
    #include <stdlib.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <unistd.h>
    #include <sys/ioctl.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netinet/tcp.h>
    #include <arpa/inet.h>

    #define SOCKET int
    #define INVALID_SOCKET (-1)
    #define SOCKET_ERROR (-1)
    #define SOCKET_REFUSED (0)
    #define SOCKET_INPROGRESS EINPROGRESS
    #define SOCKET_LASTERROR errno
#endif

#if defined(_DEBUG)
    #define F_DEBUG
#endif

#ifdef F_DEBUG
    // will help us to eleminate some warnings on some platforms with constant values
    inline bool _constWrap(const bool value) { return !!value; }

    #define	F_ASSERT(value, message, ...) if (_constWrap(!(value))) { F_BREAK(); }
#else
    #define	F_ASSERT(value, message, ...)
#endif

#define F_UNUSED(x) (void)(x)

namespace fsh
{
	template <class T, class F>
	T checked_numcast(F value)
	{
		T result = static_cast<T>(value);
		F_ASSERT(static_cast<F>(result) == value, "Cast failed");
		return result;
	}
}
