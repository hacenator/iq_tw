#pragma once

#include "common.h"
#include "singleton.h"
#include "socket.h"
#include <vector>

#ifdef F_PLATFORM_WINDOWS
#include <WinSock2.h>
#endif

namespace fsh
{
    
enum class ESocketError
{
    IncomingConnectionFailed,
    OutcomingConnectionFailed,
};
    
class ISocketsListener
{
public:
    virtual void onIncomingConnection(Socket* connection) = 0;
    virtual void onOutcomingConnection(Socket* connection) = 0;
    
    virtual void onConnectionClosed(Socket* connection) = 0;

    virtual void onIncomingData(Socket* connection) = 0;
    virtual void onDataReceived(Socket* connection, const void* data, const size_t length) = 0;
    
    virtual void onDataSent(Socket* connection) = 0;
    
    virtual void onError(Socket* connection, ESocketError reason) = 0;
};
    
class SocketManager : 
    public Singleton<SocketManager>
{
public:
    SocketManager();
    ~SocketManager();

    void init(size_t maxSockets, ISocketsListener* listener);
    void destroy();

    Socket* createSocket();
    void destroySocket(Socket* socket);

    void update();

    template <typename C>
    void foreach(const C& callback)
    {
        const size_t nSockets(_sockets.size());
        for (size_t i(0); i < nSockets; ++i)
        {
            Socket* s(_sockets[i]);
            if (s)
                callback(s);
        }
    }

    Socket* getSocket(const SocketID& id) const
    {
        Socket* socket = _sockets[id._slot];
        if (socket && socket->_id == id)
            return socket;
        else
            return nullptr;
    }
private:
    typedef std::vector<Socket*> SocketsVector;

    uint16_t _socketsCounter;
    size_t _maxSockets;
    bool _initialized;
    SocketsVector _sockets;

    ISocketsListener* _listener;
    
#ifdef F_PLATFORM_WINDOWS
    WSADATA _data;
#endif
};

}
