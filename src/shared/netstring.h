#pragma once

#include <string>

namespace fsh
{

class NetString
    : public std::u16string
{
public:
	NetString();
	NetString(const std::wstring& value);
	NetString(const wchar_t* value);
	NetString(const std::u16string& value);
	~NetString();
    
    std::wstring toWString() const;
};
    
}
