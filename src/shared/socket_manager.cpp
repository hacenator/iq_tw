#include "socket_manager.h"

#include <algorithm>

#include "socket.h"

fsh::SocketManager::SocketManager()
    : _initialized(false)
    , _maxSockets(0)
    , _listener(nullptr)
    , _socketsCounter(0)
{

}

fsh::SocketManager::~SocketManager()
{

}

void fsh::SocketManager::init(size_t maxSockets, ISocketsListener* listener)
{
    F_ASSERT(!_initialized, "Socket manager already initilized");

    if (!_initialized)
    {
        _listener = listener;
        
        _maxSockets = maxSockets;
        _sockets.resize(_maxSockets);

#ifdef F_PLATFORM_WINDOWS
        WORD version = MAKEWORD(2, 2);
        int error = WSAStartup(version, &_data);

        F_ASSERT(error == 0, "Failed to initialize winsock");

        _initialized = (error == 0);
#else
        _initialized = true;
#endif
    }
}

void fsh::SocketManager::destroy()
{
    if (_initialized)
    {
        const size_t nSockets(_sockets.size());
        for (size_t i(0); i < nSockets; ++i)
        {
            Socket* socket(_sockets[i]);
            if (socket)
            {
                socket->close();
                delete socket;
            }
        }
        _sockets.clear();

#ifdef F_PLATFORM_WINDOWS
        WSACleanup();
#endif
        
        _initialized = false;
        _listener = nullptr;
    }
}

fsh::Socket* fsh::SocketManager::createSocket()
{
    F_ASSERT(_initialized, "Socket manager not initialized");
    if (!_initialized)
        return nullptr;

    Socket* newSocket = nullptr;

    SocketsVector::iterator it = std::find(_sockets.begin(), _sockets.end(), nullptr);

    if (it != _sockets.end())
    {
        uint16_t id = checked_numcast<uint16_t>(it - _sockets.begin());
        newSocket = new Socket();
        newSocket->_id._slot = id;
        newSocket->_id._uid = _socketsCounter;
        _socketsCounter++;

        _sockets[id] = newSocket;
    }

    return newSocket;
}

void fsh::SocketManager::destroySocket(fsh::Socket* socket)
{
    F_ASSERT(socket != nullptr, "Socket can not be nullptr");

    F_ASSERT(_initialized, "Socket manager not initialized");
    if (!_initialized)
        return;

    SocketsVector::iterator it = std::find(_sockets.begin(), _sockets.end(), socket);

    F_ASSERT(it != _sockets.end(), "Specified socket not in the list");

    if (it != _sockets.end())
    {
        (*it)->close();
        delete (*it);

        _sockets[it - _sockets.begin()] = nullptr;
    }
}

void fsh::SocketManager::update()
{
    int ndfs = 0;
    
    fd_set readfds;
    fd_set writefds;
    fd_set exceptfds;

    timeval timeout;
    timeout.tv_sec = 0; 
    timeout.tv_usec = 500000;

    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);

    int descriptorsCount = 0;
    
    const size_t nSockets(_sockets.size());
    for (size_t i(0); i < nSockets; ++i)
    {
        Socket* socket(_sockets[i]);
        if (socket)
        {
            const ESocketState state(socket->getState());
            
            if (state == ESocketState::Listening ||
                state == ESocketState::Connecting ||
                state == ESocketState::Reading ||
                state == ESocketState::Writing ||
                state == ESocketState::Idle)
            {
                const SOCKET handle(socket->getHandle());
                
                FD_SET(handle, &writefds);
                FD_SET(handle, &readfds);
                FD_SET(handle, &exceptfds);
                
#if defined(F_PLATFORM_MAC)
                if (handle > ndfs)
                    ndfs = handle;
#endif
                
                descriptorsCount++;
            }
        }
    }
    
    if (descriptorsCount)
    {
        int status = select(ndfs + 1,
            &readfds,
            &writefds,
            &exceptfds,
            &timeout);

        if (status == 0)
        {
            // timeout
        }
        else if (status == SOCKET_ERROR)
        {
            int error = SOCKET_LASTERROR;
            
            F_ASSERT(false, "Failed to updated sockets");
            F_UNUSED(error);
        }
        else
        {
            for (size_t i(0); i < nSockets; ++i)
            {
                Socket* socket(_sockets[i]);
                if (socket)
                {
                    const SOCKET handle(socket->getHandle());
                    const ESocketState state(socket->getState());

                    if (FD_ISSET(handle, &readfds))
                    {
                        if (state == ESocketState::Listening)
                        {
                            Socket* newSocket = createSocket();
                            bool result = newSocket->accept(socket);
                            
                            if (result)
                            {
                                if (_listener)
                                    _listener->onIncomingConnection(newSocket);
                            }
                            else
                            {
                                if (_listener)
                                    _listener->onError(nullptr, ESocketError::IncomingConnectionFailed);
                                destroySocket(newSocket);
                            }
                        }
                        else if (state == ESocketState::Idle)
                        {
                            if (_listener)
                                _listener->onIncomingData(socket);
                            
                            // is socket still alive?
                            socket = _sockets[i];
                            if (socket)
                            {
                                ssize_t received = socket->read();
                                if (received == SOCKET_REFUSED)
                                {
                                    if (_listener)
                                        _listener->onConnectionClosed(socket);
                                }
                                else if (received != SOCKET_ERROR)
                                {
                                    if (_listener)
                                        _listener->onDataReceived(socket, socket->_incomingBuffer, received);
                                }
                            }
                        }
                        else
                        {
                            socket->_state = ESocketState::Idle;
                        }
                        status--;
                    }
                    else if (FD_ISSET(handle, &writefds))
                    {
                        socket->_state = ESocketState::Idle;
                        if (state == ESocketState::Connecting)
                        {
                            if (_listener)
                                _listener->onOutcomingConnection(socket);
                        }
                        else if (state == ESocketState::Writing)
                        {
                            socket->_state = ESocketState::Idle;
                            
                            if (socket->_toSend)
                            {
                                ssize_t result = socket->write();
                                F_UNUSED(result);
                                F_ASSERT(result != SOCKET_ERROR, "Failed to send");
                            }
                        }
                        status--;
                    }
                    else if (FD_ISSET(handle, &exceptfds))
                    {
                        if (state == ESocketState::Connecting)
                        {
                            socket->_state = ESocketState::ConnectionFailed;
                            if (_listener)
                                _listener->onError(socket, ESocketError::OutcomingConnectionFailed);
                        }
                        else
                        {
                            F_ASSERT(false, "Undefined behaviour");
                        }
                        
                        status--;
                    }
                }
                
                if (status == 0)
                    break;
            }
            
            //F_ASSERT(status == 0, "Something goes wrong");
        }
    }
}
