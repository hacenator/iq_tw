#pragma once

#include "visitor.h"

namespace fsh
{

class NetworkVisitor final
	: public IVisitor
{
public:
	explicit NetworkVisitor(size_t size = 1024);
	NetworkVisitor(const void* data, size_t size);
	~NetworkVisitor();

	void* getData() const
	{
		return _data;
	}

	size_t getSize() const
	{
		return _offset;
	}

	void reset();

private:
    void accept(const void* data, size_t size) override;
    void accept(void* data, size_t size) override;

	size_t _offset;
	size_t _bufferSize;
	char* _data;
};

}
