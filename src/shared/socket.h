#pragma once

#include "common.h"
#include <functional>


namespace fsh
{

/*
    Socket uid
*/
struct SocketID
{
    SocketID()
        : _slot(0)
        , _uid(0)
    {
    }

    SocketID(uint16_t slot, uint16_t uid)
        : _slot(slot)
        , _uid(uid)
    {
    }

    SocketID(const SocketID& other)
        : _slot(other._slot)
        , _uid(other._uid)
    {
    }

    SocketID(SocketID&& other)
        : _slot(std::move(other._slot))
        , _uid(std::move(other._uid))
    {
    }

    SocketID operator=(const SocketID& other)
    {
        _slot = other._slot;
        _uid = other._uid;

        return (*this);
    }

    SocketID operator=(SocketID&& other)
    {
        _slot = std::move(other._slot);
        _uid = std::move(other._uid);

        return (*this);
    }

    uint16_t _slot;
    uint16_t _uid;
};

inline bool operator==(const SocketID& socketA, const SocketID& socketB)
{
	return (socketA._slot == socketB._slot && socketA._uid == socketB._uid);
}

inline bool operator!=(const SocketID& socketA, const SocketID& socketB)
{
	return !(socketA == socketB);
}

/*
    Socket states
*/
enum class ESocketState : uint8_t
{
    Closed,
    Opened,
    Binded,
    
    Listening,
    Connecting,
    ConnectionFailed,
    Reading,
    Writing,
    Idle,
    Refused,
};

/*
    Socket type
*/
enum class ESocketType : uint8_t
{
    Disabled,
    Incoming, 
    Outcoming
};

/*
    Unified socket class for BSD and Windows types of sockets
*/
class Socket
{
friend class SocketManager;
// I don't like to use "friend class", but in that case it's looks like best solution.
public:
    typedef std::function<void(const void* buffer, const size_t size)> OnReceivedCallback;
    //typedef std::function<void(void* buffer, const size_t size)> OnSendCallback;
    
    Socket(size_t buffersSize = 1024);
    ~Socket();

    SocketID getId() const
    {
        return _id;
    }
    
    SOCKET getHandle() const
    {
        return _socket;
    }

    ESocketState getState() const
    {
        return _state;
    }

    ESocketType getType() const
    {
        return _type;
    }
    
    bool isConnected() const
    {
        return (_state == ESocketState::Reading ||
                _state == ESocketState::Writing ||
                _state == ESocketState::Idle);
    }
    
    bool isReady() const
    {
        return (_state == ESocketState::Idle);
    }
    
    bool isListener() const
    {
        return (_state == ESocketState::Listening);
    }
    
    void open();
    void close();

    bool listen(const char* addr, uint16_t port);
    bool connect(const char* addr, uint16_t port);
    bool accept(Socket* socket);
    
    bool send(const void* buffer, size_t size);
    
    void setOnReceivedCallback(const OnReceivedCallback&& callback)
    {
        _onReceived = callback;
    }
    
private:
    struct SockAddr
    {
        union
        {
            sockaddr_in  ipv4;
            sockaddr_in6 ipv6;
        } sa;
    };
    
    SockAddr getSockAddrFor(const char* addr, uint16_t port);

    void initBuffers();
    
    // sockets IO
    ssize_t read();
    ssize_t write();
    
    SocketID _id;
    
    const size_t _buffersSize;
    ESocketState _state;
    ESocketType _type;
    SOCKET _socket;
    
    void* _incomingBuffer;
    void* _outcomingBuffer;
    size_t _toSend;
    
    OnReceivedCallback _onReceived;
};

}
