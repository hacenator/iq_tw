#pragma once

#include "common.h"
#include "message.h"

namespace fsh
{

class MessageChat final :
    public Message
{
public:
    MessageChat()
        : Message(EMessageType::NET_MESSAGE_CHATMESSAGE)
    {
    }

    MessageChat(const MessageChat& other)
        : Message(EMessageType::NET_MESSAGE_CHATMESSAGE)
        , _authorName(other._authorName)
        , _message(other._message)
    {
    }

    MessageChat(MessageChat&& other)
        : Message(EMessageType::NET_MESSAGE_CHATMESSAGE)
        , _authorName(std::move(other._authorName))
        , _message(std::move(other._message))
    {
    }

    MessageChat operator=(const MessageChat& other)
    {
        _authorName = other._authorName;
        _message = other._message;

        return (*this);
    }

    void setAuthorName(const NetString& name)
    {
        _authorName = name;
    }

    const NetString& getAuthorName() const
    {
        return _authorName;
    }

    void setText(const NetString& text)
    {
        _message = text;
    }

    const NetString& getText() const
    {
        return _message;
    }

    void visit(IVisitor* visitor) override
    {
        visitor->accept(_authorName);
        visitor->accept(_message);
    }

private:
    NetString _authorName;
    NetString _message;
};

}
