#pragma once

#include "common.h"
#include "socket_manager.h"
#include "message_factory.h"
#include "protocol.h"

#include <set>
#include <queue>

class SocketsListener;

namespace fsh
{
    
class Peer
{
public:
    Peer();
    ~Peer();

    virtual void init();
    virtual void destroy();
    virtual void update();

    // accessors
    MessageFactory* getMessageFactory() const
    {
        return _messageFactory;
    }

    SocketManager* getSocketManager() const
    {
        return _socketManager;
    }
    
    // virtual
    virtual void registerIncomingMessage(Message* msg);
    virtual void registerOutcomingMessage(Message* msg);
    virtual void registerProtocol(Protocol* protocol);
    
    virtual void onClientConnected(const PeerID& peer) { F_UNUSED(peer); }
    virtual void onClientDisconnected(const PeerID& peer) { F_UNUSED(peer); }
    
    virtual void onConnected(const PeerID& peer) { F_UNUSED(peer); }
    virtual void onDisconnected(const PeerID& peer) { F_UNUSED(peer); }
    
private:
    typedef std::set<IProtocol*> ProtocolsSet;

    bool processMessage(const PeerID& peer, const Message* msg);

    SocketManager* _socketManager;
    SocketsListener* _socketsListener;

    MessageFactory* _messageFactory;

    std::set<IProtocol*> _protocols;
    std::queue<Message*> _queueIncoming;
    std::queue<Message*> _queueOutcoming;
};

}
