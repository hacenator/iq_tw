#include "message_factory.h"

#include <algorithm>

// messages
#include "join_message.h"
#include "chat_message.h"

fsh::MessageFactory::MessageFactory(uint32_t size /*= 1024*/)
{
    _messages.resize(size);
}

fsh::MessageFactory::~MessageFactory()
{
    std::for_each(_messages.begin(), _messages.end(), [](Message* msg)
    {
        delete msg;
    });
    _messages.clear();
}

fsh::Message* fsh::MessageFactory::allocMessage(fsh::EMessageType type)
{
    Message* newMessage = nullptr;

    MessagesVector::iterator it = std::find(_messages.begin(), _messages.end(), nullptr);

    if (it != _messages.end())
    {
        uint32_t id = checked_numcast<uint32_t>(it - _messages.begin());

        newMessage = createMessage(type);
        _messages[id] = newMessage;
    }

    F_ASSERT(newMessage != nullptr, "Failed to initialize message");

    return newMessage;
}

void fsh::MessageFactory::freeMessage(Message* msg)
{
    F_ASSERT(msg != nullptr, "Message can not be nullptr");

    MessagesVector::iterator it = std::find(_messages.begin(), _messages.end(), msg);

    F_ASSERT(it != _messages.end(), "Specified message not found");
    if (it != _messages.end())
    {
        uint32_t id = checked_numcast<uint32_t>(it - _messages.begin());
        delete msg;
        _messages[id] = nullptr;
    }
}

fsh::Message* fsh::MessageFactory::createMessage(fsh::EMessageType type)
{
    #define REGISTER_MESSAGE_TYPE(type, classname) \
            case type: return new classname();

    switch (type)
    {
        REGISTER_MESSAGE_TYPE(EMessageType::NET_MESSAGE_JOINREQUEST, MessageJoinRequest);
        REGISTER_MESSAGE_TYPE(EMessageType::NET_MESSAGE_JOINRESPONSE, MessageJoinResponse);
        REGISTER_MESSAGE_TYPE(EMessageType::NET_MESSAGE_CHATMESSAGE, MessageChat);
        default:
            F_ASSERT(0, "Failed to create mesasge of specified type");
            return nullptr;
    }

    #undef REGISTER_MESSAGE_TYPE
}
