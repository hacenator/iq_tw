#pragma once

#include <stdint.h>
#include <utility>

namespace fsh
{
	// types
	struct float2
	{
		float2() : x(0), y(0) { }
		float2(float x, float y) : x(x), y(y) { }
		float2(const float2& other) : x(other.x), y(other.y) { }
		float2 operator=(const float2& other) { x = other.x; y = other.y; return (*this); }
		float2(float2&& other) : x(other.x), y(other.y) { }
		float2 operator=(float2&& other) { x = other.x; y = other.y; return (*this); }

		float x;
		float y;
	};

	struct float3
	{
		float3() : x(0), y(0), z(0) { }
		float3(float x, float y, float z) : x(x), y(y), z(z) { }
		float3(const float3& other) : x(other.x), y(other.y), z(other.z) { }
		float3 operator=(const float3& other) { x = other.x; y = other.y; z = other.z; return (*this); }
		float3(float3&& other) : x(other.x), y(other.y), z(other.z) { }
		float3 operator=(float3&& other) { x = other.x; y = other.y; z = other.z; return (*this); }

		float x;
		float y;
		float z;
	};

	struct float4
	{
		float4() : x(0), y(0), z(0), w(0) { }
		float4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }
		float4(const float4& other) : x(other.x), y(other.y), z(other.z), w(other.w) { }
		float4 operator=(const float4& other) { x = other.x; y = other.y; z = other.z; w = other.w; return (*this); }
		float4(float4&& other) : x(other.x), y(other.y), z(other.z), w(other.w) { }
		float4 operator=(float4&& other) { x = other.x; y = other.y; z = other.z; w = other.w; return (*this); }

		float x;
		float y;
		float z;
		float w;
	};

	inline float4 rgbaToFloat4(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255)
	{
		return float4(
			static_cast<float>(r) / 255.0f, 
			static_cast<float>(g) / 255.0f,
			static_cast<float>(b) / 255.0f,
			static_cast<float>(a) / 255.0f);
	}

	struct Vec2TC
	{
		static const size_t stride;

		Vec2TC()
			: scissorId(0)
		{
		}

		Vec2TC(float x, float y, float u, float v, float r, float g, float b, float a, uint8_t sId = 0)
			: pos(x, y)
			, tex(u, v)
			, color(r, g, b, a)
			, scissorId(sId)
		{
		}

		Vec2TC(float x, float y, float u, float v, const float4& colorValue, uint8_t sId = 0)
			: pos(x, y)
			, tex(u, v)
			, color(colorValue)
			, scissorId(sId)
		{
		}

		Vec2TC(const Vec2TC& other)
			: pos(other.pos)
			, tex(other.tex)
			, color(other.color)
			, scissorId(other.scissorId)
		{
		}

		Vec2TC(Vec2TC&& other)
			: pos(std::move(other.pos))
			, tex(std::move(other.tex))
			, color(std::move(other.color))
			, scissorId(other.scissorId)
		{
		}

		Vec2TC operator=(const Vec2TC& other)
		{
			pos = other.pos;
			tex = other.tex;
			color = other.color;
			scissorId = other.scissorId;

			return (*this);
		}

		Vec2TC operator=(Vec2TC&& other)
		{
			pos = std::move(other.pos);
			tex = std::move(other.tex);
			color = std::move(other.color);
			scissorId = other.scissorId;

			return (*this);
		}

		float2 pos;
		float2 tex;
		float4 color;
		int scissorId;
	};

	struct Vec2C
	{
		static const size_t stride;

		Vec2C()
		{
		}

		Vec2C(float x, float y, float r, float g, float b, float a)
			: pos(x, y)
			, color(r, g, b, a)
		{
		}

		Vec2C(float x, float y, const float4& colorValue)
			: pos(x, y)
			, color(colorValue)
		{
		}

		Vec2C(const Vec2C& other)
			: pos(other.pos)
			, color(other.color)
		{
		}

		Vec2C(Vec2C&& other)
			: pos(std::move(other.pos))
			, color(std::move(other.color))
		{
		}

		Vec2C operator=(const Vec2C& other)
		{
			pos = other.pos;
			color = other.color;

			return (*this);
		}

		Vec2C operator=(Vec2C&& other)
		{
			pos = std::move(other.pos);
			color = std::move(other.color);

			return (*this);
		}

		float2 pos;
		float4 color;
	};
}
