#include "netstring.h"

#include "common.h"
#include <locale>
#include <memory>
#include <codecvt>

namespace
{
	template <typename F, typename T>
	std::codecvt_base::result convert(size_t size, const F* in, std::basic_string<T/*, std::char_traits<T>, std::allocator<T>*/>& out)
	{
		typedef std::codecvt<T, F, std::mbstate_t> conv;
		
		std::locale locale;
		std::mbstate_t state = std::mbstate_t();

		const conv::extern_type* from_next;
		conv::intern_type* to_next;

		out.clear();
		out.assign(size, 0);
		auto result = std::use_facet<conv>(locale).in(
			state,
			&in[0], &in[size], from_next,
			&out[0], &out[size], to_next);

		return result;
	}
}

fsh::NetString::NetString()
{

}

fsh::NetString::NetString(const std::wstring& value)
{
	auto result = convert<wchar_t, char16_t>(value.size(), value.c_str(), (*this));
	F_UNUSED(result);
	F_ASSERT(result == std::codecvt_base::ok, "Failed to convert string");
}

fsh::NetString::NetString(const wchar_t* value)
{
	auto result = convert<wchar_t, char16_t>(wcslen(value), value, (*this));
	F_UNUSED(result);
	F_ASSERT(result == std::codecvt_base::ok, "Failed to convert string");
}

fsh::NetString::NetString(const std::u16string& value)
	: std::u16string(value)
{

}

fsh::NetString::~NetString()
{
	clear();
}

std::wstring fsh::NetString::toWString() const
{
	std::wstring ws;
	auto result = convert<char16_t, wchar_t>(size(), c_str(), ws);
	F_UNUSED(result);
	F_ASSERT(result == std::codecvt_base::ok, "Failed to convert string");

	return ws;
}
