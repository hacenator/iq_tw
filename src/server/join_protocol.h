#pragma once

#include <common.h>
#include <protocol.h>
#include <join_message.h>

namespace fsv
{

class ProtocolJoin final
	: public fsh::Protocol
{
public:
	ProtocolJoin(fsh::Peer* handler);
	~ProtocolJoin();

	fsh::EProtocolResult processMessage(const fsh::PeerID& peer, const fsh::Message* msg) override;

private:
    void sendResponse(const fsh::PeerID& peer, fsh::MessageJoinResponse::EJoinResult code);
};

}
