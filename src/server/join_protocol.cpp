#include "join_protocol.h"

#include <peer.h>

fsv::ProtocolJoin::ProtocolJoin(fsh::Peer* handler)
    : fsh::Protocol(handler)
{

}

fsv::ProtocolJoin::~ProtocolJoin()
{

}

fsh::EProtocolResult fsv::ProtocolJoin::processMessage(const fsh::PeerID& peer, const fsh::Message* message)
{
	const fsh::EMessageType type(message->getType());

	switch (type)
	{
		case fsh::EMessageType::NET_MESSAGE_JOINREQUEST:
			const fsh::MessageJoinRequest* msg = static_cast<const fsh::MessageJoinRequest*>(message);

			const std::wstring userNick(msg->getNickname().toWString().c_str());
            const uint32_t version(msg->getVersionCode());

            printf("Attempt to join: '%ls' (%u)\n", userNick.c_str(), version);
			
            if (version != F_VERSIONCODE)
                sendResponse(peer, fsh::MessageJoinResponse::EJoinResult::Failed_Version);
            else
                sendResponse(peer, fsh::MessageJoinResponse::EJoinResult::Accept);

            return fsh::EProtocolResult::COMPLETE;
	}
    
    return fsh::EProtocolResult::PASS;
}

void fsv::ProtocolJoin::sendResponse(const fsh::PeerID& peer, fsh::MessageJoinResponse::EJoinResult code)
{
    fsh::MessageJoinResponse* response = static_cast<fsh::MessageJoinResponse*>(_handler->getMessageFactory()->allocMessage(fsh::EMessageType::NET_MESSAGE_JOINRESPONSE));
    F_ASSERT(response, "Failed to allocate message");
    if (response)
    {
        response->addDstPeer(peer);
        response->setResult(code);
        _handler->registerOutcomingMessage(response);
    }
}
