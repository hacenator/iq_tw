#include <peer.h>
#include <message.h>
#include <message_factory.h>
#include <join_message.h>
#include <network_visitor.h>
#include "join_protocol.h"
#include "chat_protocol.h"

// leak detection
//#include <vld.h>

const char* sDefaultAddr = "127.0.0.1";
const uint16_t sDefaultPort = 2121;

class Server :
    public fsh::Peer
{
public:
    Server() 
        : _listener(nullptr)
    {

    }

	bool listen(const char* addr, uint16_t port)
	{
        bool result = _listener->listen(addr, port);
        return result;
	}

    void init()
    {
		Peer::init();

        _listener = getSocketManager()->createSocket();
	}

    void destroy()
    {
        getSocketManager()->destroySocket(_listener);
        _listener = nullptr;

		Peer::destroy();
    }

private:
    fsh::Socket* _listener;
};

Server server;

int main()
{
    server.init();
   
    server.registerProtocol(new fsv::ProtocolJoin(&server));
	server.registerProtocol(new fsv::ProtocolChat(&server));

    bool result = server.listen(sDefaultAddr, sDefaultPort);
    while (result)
    {
        server.update();
    }

    server.destroy();
    
    return 0;
}

