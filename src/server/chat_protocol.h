#pragma once

#include <protocol.h>

namespace fsv
{

class ProtocolChat :
    public fsh::Protocol
{
public:
    explicit ProtocolChat(fsh::Peer* handler);
    ~ProtocolChat();

    fsh::EProtocolResult processMessage(const fsh::PeerID& peer, const fsh::Message* msg) override;
};

}
