#include "chat_protocol.h"

#include <peer.h>
#include <chat_message.h>

fsv::ProtocolChat::ProtocolChat(fsh::Peer* handler)
    : fsh::Protocol(handler)
{

}

fsv::ProtocolChat::~ProtocolChat()
{

}

fsh::EProtocolResult fsv::ProtocolChat::processMessage(const fsh::PeerID& peer, const fsh::Message* message)
{
    const fsh::EMessageType type(message->getType());

    switch (type)
    {
        case fsh::EMessageType::NET_MESSAGE_CHATMESSAGE:
            const fsh::MessageChat* msg = static_cast<const fsh::MessageChat*>(message);

            printf("Message from client received: ");

            // new message
            fsh::MessageChat* bmsg = static_cast<fsh::MessageChat*>(_handler->getMessageFactory()->allocMessage(type));
            if (bmsg)
            {
                (*bmsg) = (*msg);

                // broadcast
                _handler->getSocketManager()->foreach([bmsg, peer](fsh::Socket* s)
                {
                    if (s->getType() == fsh::ESocketType::Incoming && 
						!s->isListener() &&
						s->getId() != peer)
                        bmsg->addDstPeer(s->getId());
                });

                _handler->registerOutcomingMessage(bmsg);
                
                printf("broadcasted\n");
            }
            else
            {
                printf("failed to broadcast\n");
            }

            return fsh::EProtocolResult::COMPLETE;
    }

    return fsh::EProtocolResult::PASS;
}
