#include "ui.h"

#include <GLFW/glfw3.h>

// control
void fcl::ui::Control::update()
{
	F_ASSERT(_parent, "Parent not available");

	if (_parent)
	{
		const float density(_parent->getDensity());
		const fsh::float2 parentPos(_parent->getPosPx());
		const fsh::float2 parentSize(_parent->getSizePx());

		const fsh::float4 paddingPx(_paddingDp.x * density, _paddingDp.y * density, _paddingDp.z * density, _paddingDp.w * density);

		// size
		if (_sizeDp.x == FILL_MATCH_PARENT)
		{
			_sizePx = fsh::float2(parentSize.x, _sizeDp.y * density);
		}
		else if (_sizeDp.y == FILL_MATCH_PARENT)
		{
			_sizePx = fsh::float2(_sizeDp.x * density, parentSize.y);
		}
		else
		{
			_sizePx = fsh::float2(_sizeDp.x * density, _sizeDp.y * density);
		}

		// pos
		// horizontal
		if (_gravity & GRAVITY_LEFT)
		{
			_posPx.x = parentPos.x + paddingPx.w;
		}
		else if (_gravity & GRAVITY_RIGHT)
		{
			_posPx.x = (parentPos.x + parentSize.x) - _sizePx.x - paddingPx.y;
		}
		else if (_gravity & GRAVITY_CENTERH)
		{
			_posPx.x = parentPos.x + (parentSize.x / 2.0f) - (_sizePx.x / 2.0f)/* + paddingPx.w*/;
		}
		else if (_gravity & GRAVITY_ABSOLUTEH)
		{
			_posPx.x = _posDp.x * density + paddingPx.w;
		}

		// vertical
		if (_gravity & GRAVITY_TOP)
		{
			_posPx.y = parentPos.y + paddingPx.x;
		}
		else if (_gravity & GRAVITY_BOTTOM)
		{
			_posPx.y = (parentPos.y + parentSize.y) - _sizePx.y - paddingPx.z;
		}
		else if (_gravity & GRAVITY_CENTERV)
		{
			_posPx.y = parentPos.y + (parentSize.y / 2.0f) - (_sizePx.y / 2.0f);
		}
		else if (_gravity & GRAVITY_ABSOLUTEV)
		{
			_posPx.y = (_posDp.y * density) + paddingPx.x;
		}
	}
}

bool fcl::ui::Control::hover(const fsh::float2& p)
{
	const fsh::float2 posPx2(_posPx.x + _sizePx.x, _posPx.y + _sizePx.y);
	return (p.x >= _posPx.x && p.x <= posPx2.x &&
		p.y >= _posPx.y && p.y <= posPx2.y);
}

// layout
void fcl::ui::LayoutControl::update()
{
	const size_t nControls(_controls.size());
	for (int i(0); i < nControls; ++i)
		_controls[i]->update();
}

void fcl::ui::LayoutControl::draw(const fsh::float4* const scissor /*= nullptr*/)
{
	const size_t nControls(_controls.size());
	for (int i(0); i < nControls; ++i)
		_controls[i]->draw(scissor);
}

void fcl::ui::LayoutControl::setSizePx(int x, int y)
{
	_sizePx = fsh::float2(x, y);

	update();
}

void fcl::ui::LayoutControl::onMouseMove(int x, int y)
{
	const fsh::float2 p(x, y);
	const size_t nControls(_controls.size());
	Control* hoverControl = nullptr;
	for (int i(nControls - 1); i >= 0 ; --i)
	{
		fcl::ui::Control* control(_controls[i]);

		if (control->hover(p))
		{
			control->onMouseMove(x, y);

			if (control != _controlHover)
			{
				if (_controlHover)
					_controlHover->onMouseOut();

				_controlHover = control;

				_controlHover->onMouseOver();
			}
			hoverControl = control;
			break;
		}
	}

	if (!hoverControl)
	{
		if (_controlHover)
			_controlHover->onMouseOut();
		_controlHover = nullptr;
	}
}

void fcl::ui::LayoutControl::onMouseOver()
{

}

void fcl::ui::LayoutControl::onMouseOut()
{
	if (_controlHover)
	{
		_controlHover->onMouseOut();
		_controlHover = nullptr;
	}
}

void fcl::ui::LayoutControl::onClick(int x, int y)
{
	const fsh::float2 p(x, y);
	const size_t nControls(_controls.size());
	Control* clickControl = nullptr;
	for (int i(nControls - 1); i >= 0; --i)
	{
		fcl::ui::Control* control(_controls[i]);

		if (control->hover(p))
		{
			if (_controlFocused && _controlFocused != control)
				_controlFocused->_isFocused = false;

			control->onClick(x, y);

			if (_controlFocused != control)
			{
				control->_isFocused = true;
				_controlFocused = control;
			}
			clickControl = control;
			break;
		}
	}

	if (!clickControl)
	{
		if (_controlFocused)
			_controlFocused->_isFocused = false;
		_controlFocused = nullptr;
	}
}

void fcl::ui::LayoutControl::onText(const wchar_t symbol)
{
	if (_controlFocused)
		_controlFocused->onText(symbol);
}

void fcl::ui::LayoutControl::onKey(int scancode, KeyAction action)
{
	if (_controlFocused)
		_controlFocused->onKey(scancode, action);
}


// text
void fcl::ui::TextControl::draw(const fsh::float4* const scissor /*= nullptr*/)
{
	fcl::Renderer* renderer(fcl::Renderer::getInstance());

	if (_text.length() && _textColor.w > 0.0f)
		renderer->drawText(_posPx, _text.c_str(), _textColor, scissor);
}

// panel
void fcl::ui::PanelControl::update()
{
	Control::update();
	LayoutControl::update();
}

void fcl::ui::PanelControl::draw(const fsh::float4* const scissor /*= nullptr*/)
{
	fcl::Renderer* renderer(fcl::Renderer::getInstance());

	if (_backColor.w > 0.0f)
		renderer->drawRectangle(_posPx, _sizePx, _backColor, 0.0f);

	LayoutControl::draw(scissor);
}

// button
void fcl::ui::ButtonControl::draw(const fsh::float4* const scissor /*= nullptr*/)
{
	PanelControl::draw(scissor);
}

void fcl::ui::ButtonControl::onMouseMove(int x, int y)
{

}

void fcl::ui::ButtonControl::onMouseOver()
{
	_text.setTextColor(_hoverTextColor);
}

void fcl::ui::ButtonControl::onMouseOut()
{
	_text.setTextColor(_defaultTextColor);
}

void fcl::ui::ButtonControl::onClick(int x, int y)
{
	if (_onClick)
		_onClick();
}


// textarea
fcl::ui::TextareaControl::TextareaControl()
	: _cursor(0)
	, _placeholder(false)
{
	addControl(&_text);
	_text.setPaddingDp(0, 0, 0, textPaddingX);
	_text.setGravity(GRAVITY_LEFT | GRAVITY_CENTERV);
}

void fcl::ui::TextareaControl::draw(const fsh::float4* const scissor /*= nullptr*/)
{
	F_ASSERT(scissor == nullptr, "Parent scissor will be ignored. TODO: Merge");

	fsh::float4 newScissor(_posPx.x, _posPx.y, _posPx.x + _sizePx.x, _posPx.y + _sizePx.y);
	PanelControl::draw(&newScissor);

	// draw cursor
	if (_isFocused)
	{
		const uint8_t padding(2);

		fcl::Renderer* renderer(fcl::Renderer::getInstance());

		float textWidth = 0.0f;
		if (!_placeholder)
			textWidth = renderer->textWidth(_text.getText().c_str(), _cursor);

		const fsh::float2 cursorPos(_posPx.x + textPaddingX + textWidth, _posPx.y + padding);
		if (cursorPos.x < _posPx.x + _sizePx.x)
			renderer->drawRectangle(cursorPos, fsh::float2(2, _sizePx.y - (padding * 2)), fsh::float4(0, 0, 0, 1.0f));
	}
}

void fcl::ui::TextareaControl::setText(const std::wstring& text)
{
	if (text.length())
	{
		_placeholder = false;
		_text.setText(text);
		_cursor = text.length();
	}
	else
	{
		_placeholder = true;
		_text.setText(_placeholderText);
		_cursor = 0;
	}
}

void fcl::ui::TextareaControl::onMouseOver()
{
	_text.setTextColor(_hoverTextColor);
}

void fcl::ui::TextareaControl::onMouseOut()
{
	_text.setTextColor(_defaultTextColor);
}

void fcl::ui::TextareaControl::onClick(int x, int y)
{

}

void fcl::ui::TextareaControl::onText(const wchar_t symbol)
{
	if (_placeholder)
	{
		std::wstring newString;
		newString += symbol;

		_text.setText(newString);

		_cursor = newString.length();
		_placeholder = false;
	}
	else
	{
		std::wstring text = _text.getText();
		text.insert(_cursor, 1, symbol);
		_cursor++;
		_text.setText(text);
	}
}

void fcl::ui::TextareaControl::onKey(int key, KeyAction action)
{
	if (action == KeyAction::RELEASE || action == KeyAction::REPEAT)
	{
		switch (key)
		{
			case GLFW_KEY_BACKSPACE:
				{
					if (_cursor > 0)
					{
						std::wstring text = _text.getText();
						text.replace(_cursor - 1, 1, L"");
						_text.setText(text);
						_cursor--;
					}
				}
				break;
			case GLFW_KEY_DELETE:
				{
					if (_cursor < _text.getText().length())
					{
						std::wstring text = _text.getText();
						text.replace(_cursor, 1, L"");
						_text.setText(text);
					}
				}
				break;
			case GLFW_KEY_LEFT:
				if (_cursor > 0)
					_cursor--;
				break;
			case GLFW_KEY_RIGHT:
				if (_cursor < _text.getText().length())
					_cursor++;
				break;
		}

		if (_onKeyPress)
			_onKeyPress(key);
	}
}

/* List */
void fcl::ui::TextList::draw(const fsh::float4* const scissor /*= nullptr*/)
{
	const size_t nLines(_lines.size());

	if (nLines)
	{
		F_ASSERT(scissor == nullptr, "Parent scissor will be ignored. TODO: Merge");
		fsh::float4 newScissor(_posPx.x, _posPx.y, _posPx.x + _sizePx.x, _posPx.y + _sizePx.y);

		fcl::Renderer* renderer(fcl::Renderer::getInstance());
		fsh::float2 textPosPx(_posPx.x, _posPx.y + _sizePx.y - 32);

		for (int i(nLines - 1); i >= 0; --i)
		{
			renderer->drawText(textPosPx, _lines[i].c_str(), fsh::float4(0.0f, 0.0f, 0.0f, 1.0f), &newScissor);
			textPosPx.y -= 32.0f;

			if (textPosPx.y <= _posPx.y)
				break;
		}
	}
}

void fcl::ui::TextList::addLine(const std::wstring& line)
{
	// yes. there is a memory blcakhole with unlimited messages
	_lines.push_back(line);
}
