#include "layout_manager.h"

#include "chat_layout.h"

fcl::LayoutManager::LayoutManager()
	: _currentLayout(nullptr)
{
	fcl::Renderer* renderer(fcl::Renderer::getInstance());

	_layouts[LAYOUTS_CHAT] = new ChatLayout(renderer);
	_layouts[LAYOUTS_CONNECTION] = nullptr;
}

fcl::LayoutManager::~LayoutManager()
{
	for (int i(0); i < LAYOUTS_MAX; ++i)
		delete _layouts[i];
}

void fcl::LayoutManager::onSurfaceResize(int width, int height)
{
	_surfaceWidth = width;
	_surfaceHeight = height;

	if (_currentLayout)
		_currentLayout->onSurfaceResize(width, height);
}

void fcl::LayoutManager::onMouseMove(int x, int y)
{
	_pointerX = x;
	_pointerY = y;

	if (_currentLayout)
		_currentLayout->onMouseMove(x, y);
}

void fcl::LayoutManager::onClick()
{
	if (_currentLayout)
		_currentLayout->onClick(_pointerX, _pointerY);
}

void fcl::LayoutManager::onText(const wchar_t symbol)
{
	if (_currentLayout)
		_currentLayout->onText(symbol);
}

void fcl::LayoutManager::onKey(int scancode, KeyAction action)
{
	if (_currentLayout)
		_currentLayout->onKey(scancode, action);
}

void fcl::LayoutManager::draw()
{
	if (_currentLayout)
		_currentLayout->draw();
}

void fcl::LayoutManager::enableLayout(ELayout layout)
{
	if (layout == LAYOUTS_NONE)
		setLayout(nullptr);
	else
		setLayout(_layouts[layout]);
}

void fcl::LayoutManager::setLayout(Layout* stage)
{
	if (_currentLayout)
		_currentLayout->onHide();

	_currentLayout = stage;

	if (_currentLayout)
	{
		_currentLayout->onSurfaceResize(_surfaceWidth, _surfaceHeight);
		_currentLayout->onShow();
	}
}
