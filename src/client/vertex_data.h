namespace fcl
{

#include <common.h>
#include <types.h>

class IVertexData
{
public:
	virtual ~IVertexData()
	{
	}

	virtual size_t getLength() const = 0;
	virtual size_t getSize() const = 0;
	virtual size_t getUsedLength() const = 0;
	virtual void* getData() const = 0;
};

template <class Layout>
class VertexData
	: public IVertexData
{
public:
	VertexData(size_t length)
		: _length(length)
		, _used(0)
	{
		_data = static_cast<Layout*>(malloc(sizeof(Layout) * length));
	}

	~VertexData()
	{
		free(_data);
	}

	void push(const Layout& element)
	{
		if (_used < _length)
		{
			_data[_used] = element;
			_used++;
		}
	}

	/* Vec2C shorthands */
	template<typename L = Layout>
	typename std::enable_if<std::is_same<L, fsh::Vec2C>::value, void>::type
	push(float x, float y, const fsh::float4& color)
	{
		push(fsh::Vec2C(x, y, color));
	}

	/* Vec2TC shorthands */
	template<typename L = Layout>
	typename std::enable_if<std::is_same<L, fsh::Vec2TC>::value, void>::type
	push(float x, float y, float u, float v, const fsh::float4& color, int scissorId = 0)
	{
		push(fsh::Vec2TC(x, y, u, v, color, scissorId));
	}

	void reset()
	{
		_used = 0;
	}

	size_t getLength() const override
	{
		return _length;
	}

	size_t getSize() const override
	{
		return _length * sizeof(Layout);
	}

	size_t getUsedLength() const override
	{
		return _used;
	}

	void* getData() const override
	{
		return _data;
	}

private:
	size_t _length;
	size_t _used;

	Layout* _data;
};

}