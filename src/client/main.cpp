#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "chat_layout.h"
#include "client.h"

// leak detection
//#include <vld.h>

namespace
{
	static const int gSurfaceWidth = 640;
	static const int gSurfaceHeight = 480;
}

void onResize(GLFWwindow* /*window*/, int width, int height)
{
	fcl::Renderer::getInstance()->onSurfaceResize(width, height);
	fcl::LayoutManager::getInstance()->onSurfaceResize(width, height);
}

void onKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	fcl::LayoutManager::getInstance()->onKey(key, static_cast<fcl::KeyAction>(action));
}

void onText(GLFWwindow* window, unsigned int codepoint)
{
	fcl::LayoutManager::getInstance()->onText(codepoint);
}

void onPointer(GLFWwindow* /*window*/, double x, double y)
{
	fcl::LayoutManager::getInstance()->onMouseMove(x, y);
}

void onTouch(GLFWwindow* /*window*/, int button, int action, int /*mods*/)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		fcl::LayoutManager::getInstance()->onClick();
}

#ifdef F_PLATFORM_WINDOWS
int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
)
#else
int main()
#endif
{
	if (!glfwInit())
		return 1;

	GLFWwindow* window = nullptr;
	window = glfwCreateWindow(gSurfaceWidth, gSurfaceHeight, "FMChat", nullptr, nullptr);
	F_ASSERT(window, "Failed to create window");
	if (!window)
		return 1;

	// Set callback functions
	glfwSetFramebufferSizeCallback(window, onResize);
	glfwSetKeyCallback(window, onKey);
	glfwSetCharCallback(window, onText);
	glfwSetCursorPosCallback(window, onPointer);
	glfwSetMouseButtonCallback(window, onTouch);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

    fcl::Client* client(fcl::Client::getInstance());
	fcl::Renderer* renderer(fcl::Renderer::getInstance());
	fcl::LayoutManager* layoutManager(fcl::LayoutManager::getInstance());

    client->init();
	renderer->init();
	renderer->onSurfaceResize(gSurfaceWidth, gSurfaceHeight);

	client->setOnJoin([]()
	{
		fcl::LayoutManager* layoutManager(fcl::LayoutManager::getInstance());
		layoutManager->enableLayout(fcl::LayoutManager::LAYOUTS_CHAT);
	});

	client->setOnLeave([]()
	{
		fcl::LayoutManager* layoutManager(fcl::LayoutManager::getInstance());
		layoutManager->enableLayout(fcl::LayoutManager::LAYOUTS_CHAT);
	});

	client->setOnMessage([](const std::wstring& msg)
	{
		fcl::LayoutManager* layoutManager(fcl::LayoutManager::getInstance());
		fcl::ChatLayout* chat = static_cast<fcl::ChatLayout*>(layoutManager->getLayout(fcl::LayoutManager::LAYOUTS_CHAT));
		chat->addMessage(msg);
	});

    bool result = client->connect("127.0.0.1", 2121);
    F_ASSERT(result, "Connection failed");
	result;

    bool ready = false;
    while (!glfwWindowShouldClose(window))
    {
        client->update();

		// draw
		layoutManager->draw();
		renderer->update();

		// GLFW Present
		glfwSwapBuffers(window);
		glfwPollEvents();
    }

	renderer->destroy();
    client->destroy();

	fcl::LayoutManager::shutdown();
	fcl::Renderer::shutdown();
	fcl::Client::shutdown();

	glfwTerminate();

    return 0;
}