const char* gShaderTextV =
R"STR(#version 330

//uniform mat4 WorldMatrix;
uniform vec2 screenSize;
uniform vec4 scissorBuffers[16];

// VS In
layout(location = 0) in vec2 inPosition0;
layout(location = 1) in vec2 inTexCoord0;
layout(location = 2) in vec4 inColor;
layout(location = 3) in int  inScissorId;

// VS Out
out vec2 psTexCoord0;
out vec4 psColor;
out vec4 psScissor;

void main()
{
	psScissor = scissorBuffers[inScissorId];
	psTexCoord0 = inTexCoord0;

	psColor = inColor;

	gl_Position = vec4(inPosition0.xy * vec2(2.0 / screenSize.x, 2.0 / -screenSize.y) + vec2(-1, 1), 0.0f, 1.0f);
}

)STR";

const char* gShaderTextP =
R"STR(#version 150

// uniforms
uniform sampler2D tex1;

// PS In
in vec2 psTexCoord0;
in vec4 psColor;
in vec4 psScissor;

// PS Out
out vec4 outColor;

void main()
{    
	float a = texture(tex1, psTexCoord0).a;

	if (gl_FragCoord.x < psScissor.x || gl_FragCoord.x > psScissor.z ||
		gl_FragCoord.y < psScissor.y || gl_FragCoord.y > psScissor.w)
		discard;

	outColor = vec4(psColor.r, psColor.g, psColor.b, a * psColor.a);
}

)STR";

const char* gShaderColorP =
R"STR(#version 150

// uniforms
uniform sampler2D tex1;

// PS In
in vec4 psColor;

// PS Out
out vec4 outColor;

void main()
{    
	outColor = psColor;
}

)STR";
