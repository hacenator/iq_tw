#include "renderer.h"
#include "layout_manager.h"
#include "ui.h"

namespace fcl
{

class ChatLayout
	: public Layout
{
public:
	ChatLayout(Renderer* renderer);
	virtual ~ChatLayout();

	void onSurfaceResize(int width, int height) override;
	void onMouseMove(int x, int y) override;
	void onClick(int x, int y) override;

	void onText(const wchar_t symbol) override;
	void onKey(int scancode, KeyAction action) override;

	void draw() override;

	void addMessage(const std::wstring& msg);
private:
	void send();
	
	ui::LayoutControl _lSurface;
	ui::PanelControl _pHeader;
	ui::TextControl _tHeader;
	ui::PanelControl _pFooter;
	ui::TextareaControl _iMessage;
	ui::ButtonControl _bSend;
	ui::TextList _tlMessages;
};

}
