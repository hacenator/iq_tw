#include "chat_protocol.h"

#include "client.h"
#include "chat_message.h"

fcl::ProrocolChat::ProrocolChat(fsh::Peer* handler)
    : fsh::Protocol(handler)
{

}

fcl::ProrocolChat::~ProrocolChat()
{

}

fsh::EProtocolResult fcl::ProrocolChat::processMessage(const fsh::PeerID& peer, const fsh::Message* message)
{
    const fsh::EMessageType type(message->getType());

    switch (type)
    {
		case fsh::EMessageType::NET_MESSAGE_CHATMESSAGE:
			{
				const fsh::MessageChat* msg = static_cast<const fsh::MessageChat*>(message);

				fcl::Client* client(fcl::Client::getInstance());

				client->onMessage(msg->getText().toWString());
			}
			return fsh::EProtocolResult::COMPLETE;
    }

    return fsh::EProtocolResult::PASS;
}
