#include "join_protocol.h"

#include "client.h"
#include <join_message.h>

fcl::ProtocolJoin::ProtocolJoin(fsh::Peer* handler)
    : Protocol(handler)
{

}

fcl::ProtocolJoin::~ProtocolJoin()
{

}

fsh::EProtocolResult fcl::ProtocolJoin::processMessage(const fsh::PeerID& peer, const fsh::Message* message)
{
	const fsh::EMessageType type(message->getType());
	
	switch (type)
	{
        case fsh::EMessageType::NET_MESSAGE_JOINRESPONSE:
            const fsh::MessageJoinResponse* msg = static_cast<const fsh::MessageJoinResponse*>(message);

            fcl::Client* client(fcl::Client::getInstance());

            switch (msg->getResult())
            {
                case fsh::MessageJoinResponse::EJoinResult::Accept:
                    client->onJoin();
                    break;

                case fsh::MessageJoinResponse::EJoinResult::Failed_Version:
                case fsh::MessageJoinResponse::EJoinResult::Failed_Other:
                case fsh::MessageJoinResponse::EJoinResult::Failed_UserAlreadyExist:
                    client->onLeave();
                    break;

                default:
                    break;
            }

            return fsh::EProtocolResult::COMPLETE;
	}

	return fsh::EProtocolResult::PASS;
}
