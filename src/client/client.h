#include <peer.h>

#include <singleton.h>
#include <functional>

#include "renderer.h"

namespace fcl
{

class Client final
    : public fsh::Singleton<Client>
    , public fsh::Peer
{
public:
    Client();
    ~Client();

    bool connect(const char* addr, uint16_t port);
    void init() override;
    void destroy() override;

    //
    void sendText(const wchar_t* text);

    // events
    void onConnected(const fsh::PeerID& peer) override;
    void onDisconnected(const fsh::PeerID& peer) override;

    void onJoin();      //< client joint to "chat room"
    void onLeave();     //< client leave "chat room"
	void onMessage(const std::wstring& message);

	bool isJoined() const
	{
		return _isJoined;
	}

	// callbacks
	void setOnJoin(std::function<void()> func)
	{
		_onJoin = func;
	}

	void setOnLeave(std::function<void()> func)
	{
		_onLeave = func;
	}

	void setOnMessage(std::function<void(const std::wstring& msg)> func)
	{
		_onMessage = func;
	}

private:
	bool _isJoined;

    fsh::Socket* _clientSocket;
    fsh::PeerID _serverPeer;

	// callbacks
	std::function<void()> _onJoin;
	std::function<void()> _onLeave;
	std::function<void(const std::wstring& message)> _onMessage;
};

}
