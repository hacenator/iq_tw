#include "renderer.h"

#include "layout_manager.h"

#include <GL/glew.h>

#define STB_TRUETYPE_IMPLEMENTATION
#include <stb/stb_truetype.h>

namespace
{
	static const char* gFontFile = "c:/windows/fonts/impact.ttf";
	static const int gFontTextureSize = 512;
	static const float gFontSize = 28.0f;

	static const int gTextBufferLength = 1024;
	static const int gUiBufferLength = 1024;

	#include "shaders.inl"

#ifdef F_DEBUG
	void GLAPIENTRY debugCallback(GLenum /*source*/, GLenum /*type*/, GLuint /*id*/, GLenum /*severity*/, GLsizei /*length*/, const GLchar* message, const void* /*userParam*/)
	{
		OutputDebugStringA(message);
		F_ASSERT(0, "Watch at message");
	}
#endif
}

fcl::Renderer::Renderer()
	: _textBuffer(gTextBufferLength)
	, _uiBuffer(gUiBufferLength)
	, _currentLayout(VertexLayouts::NONE)
	, _currentBlendMode(BlendMode::NONE)
	, _currentBuffer(Buffers::BUFFERS_NONE)
	, _currentShader(Shaders::SHADER_NONE)
	, _surfaceSize()
	, _scissorLast(1)
{

}

fcl::Renderer::~Renderer()
{

}

void fcl::Renderer::init()
{
	glewInit();

	/* font */
	uint8_t* ttf_buffer = nullptr;
	uint8_t* fontBitmap = nullptr;

	// just to safe space in stack
	fontBitmap = static_cast<uint8_t*>(malloc(sizeof(uint8_t) * (512 * 512)));

	FILE* fontFile = nullptr;
#ifdef F_PLATFORM_WINDOWS
	fopen_s(&fontFile, gFontFile, "rb");
#else
	fontFile = fopen(gFontFile, "rb");
#endif
	fseek(fontFile, 0, SEEK_END);
	long fileSize = ftell(fontFile);
	fseek(fontFile, 0, SEEK_SET);

	ttf_buffer = static_cast<uint8_t*>(malloc(fileSize));

	fread(ttf_buffer, 1, fileSize, fontFile);
	fclose(fontFile);

	stbtt_BakeFontBitmap(ttf_buffer, 0, gFontSize, fontBitmap, gFontTextureSize, gFontTextureSize, 32, 96, _fontData);

	free(ttf_buffer);
	ttf_buffer = nullptr;

	// OpenGL debug
#ifdef F_DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

	glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_FALSE);
	glDebugMessageControlARB(GL_DONT_CARE, GL_DEBUG_TYPE_ERROR, GL_DONT_CARE, 0, NULL, GL_TRUE);
	glDebugMessageControlARB(GL_DONT_CARE, GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, GL_DONT_CARE, 0, NULL, GL_TRUE);
	glDebugMessageControlARB(GL_DONT_CARE, GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, GL_DONT_CARE, 0, NULL, GL_TRUE);
	glDebugMessageCallback(debugCallback, nullptr);
#endif

	// vao required
	glGenVertexArrays(1, &_defaultVAO);
	glBindVertexArray(_defaultVAO);

	// can free ttf_buffer at this point
	glGenTextures(1, &_fontTexID);
	glBindTexture(GL_TEXTURE_2D, _fontTexID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, gFontTextureSize, gFontTextureSize, 0, GL_ALPHA, GL_UNSIGNED_BYTE, fontBitmap);
	// can free temp_bitmap at this point
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	free(fontBitmap);
	fontBitmap = nullptr;

	// opengl buffers
	glCreateBuffers(Buffers::BUFFERS_MAX, &_buffers[0]);

	bindBuffer(GL_ARRAY_BUFFER, Buffers::BUFFERS_TEXT_VB);
	glBufferData(GL_ARRAY_BUFFER, _textBuffer.getSize(), nullptr, GL_DYNAMIC_DRAW);

	bindBuffer(GL_ARRAY_BUFFER, Buffers::BUFFERS_UI_VB);
	glBufferData(GL_ARRAY_BUFFER, _uiBuffer.getSize(), nullptr, GL_DYNAMIC_DRAW);

	// shaders
	createProgram(SHADER_TEXTURE_AND_COLOR, gShaderTextV, fsh::checked_numcast<int>(strlen(gShaderTextV)), gShaderTextP, fsh::checked_numcast<int>(strlen(gShaderTextP)));
	createProgram(SHADER_COLOR, gShaderTextV, fsh::checked_numcast<int>(strlen(gShaderTextV)), gShaderColorP, fsh::checked_numcast<int>(strlen(gShaderColorP)));

	// common params
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClearDepth(1.0f);
}

void fcl::Renderer::destroy()
{
	// free opengl resources
	for (int i(0); i < SHADER_MAX; ++i)
		glDeleteProgram(_shader[i]);

	glDeleteTextures(1, &_fontTexID);
	glDeleteBuffers(Buffers::BUFFERS_MAX, &_buffers[0]);
	glDeleteVertexArrays(1, &_defaultVAO);
}

uint32_t fcl::Renderer::addScissor(const fsh::float4& scissor)
{
	// scissor
	uint32_t scissorId = SCISSORS_UNDEFINED;
	F_ASSERT(_scissorLast < SCISSORS_MAX, "Scissors overflow");
	if (_scissorLast < SCISSORS_MAX)
	{
		// search
		for (int i(0); i < SCISSORS_MAX; ++i)
		{
			if (memcmp(&scissor, &_scissorBuffers[i], sizeof(fsh::float4)) == 0)
				return scissorId;
		}

		// add new
		scissorId = _scissorLast;
		_scissorBuffers[scissorId] = fsh::float4(scissor.x, _surfaceSize.y - scissor.w, scissor.z, _surfaceSize.y - scissor.y);
		_scissorLast++;
	}

	return scissorId;
}

void fcl::Renderer::drawText(const fsh::float2& pos, const wchar_t* text, const fsh::float4& color, const fsh::float4* scissor)
{
	float x = pos.x;
	float y = pos.y + (gFontSize * 0.8125f); // 0.8125f is the magic number for stbtt_
	stbtt_aligned_quad q;

	// scissor
	uint32_t scissorId = SCISSORS_UNDEFINED;
	if (scissor)
		scissorId = addScissor((*scissor));

	while (*text) 
	{
		if (*text >= 32 && *text < 128) 
		{
			stbtt_GetBakedQuad(_fontData, gFontTextureSize, gFontTextureSize, *text - 32, &x, &y, &q, 1);

			// triangles
			_textBuffer.push(q.x0, q.y1, q.s0, q.t1, color, scissorId);
			_textBuffer.push(q.x1, q.y1, q.s1, q.t1, color, scissorId);
			_textBuffer.push(q.x1, q.y0, q.s1, q.t0, color, scissorId);

			_textBuffer.push(q.x1, q.y0, q.s1, q.t0, color, scissorId);
			_textBuffer.push(q.x0, q.y0, q.s0, q.t0, color, scissorId);
			_textBuffer.push(q.x0, q.y1, q.s0, q.t1, color, scissorId);
		}
		++text;
	}
}

float fcl::Renderer::textWidth(const wchar_t* text, size_t length)
{
	size_t i = 0;
	float x = 0.0f;
	float y = 0.0f;
	stbtt_aligned_quad q;

	if (length == 0 || text == nullptr || text[0] == L'\0')
		return 0.0f;

	while (*text)
	{
		if (i == length)
			break;

		if (*text >= 32 && *text < 128)
		{
			stbtt_GetBakedQuad(_fontData, gFontTextureSize, gFontTextureSize, *text - 32, &x, &y, &q, 1);
		}
		++i;
		++text;
	}

	return x;
}

void fcl::Renderer::drawRectangle(const fsh::float2& pos, const fsh::float2& size, const fsh::float4& color, float rounded /*= 0.0f*/)
{
	const fsh::float2 pos2(pos.x + size.x, pos.y + size.y);

	if (rounded <= 0.0f)
	{
		_uiBuffer.push(pos.x, pos2.y, color);
		_uiBuffer.push(pos2.x, pos2.y, color);
		_uiBuffer.push(pos2.x, pos.y, color);

		_uiBuffer.push(pos2.x, pos.y, color);
		_uiBuffer.push(pos.x, pos.y, color);
		_uiBuffer.push(pos.x, pos2.y, color);
	}
	else
	{
		// TODO
	}
}

bool fcl::Renderer::createProgram(const Shaders& name, const char* vsText, int vsSize, const char* psText, int psSize)
{
	GLuint resShaderVs = compileShader(GL_VERTEX_SHADER, vsText, vsSize);
	F_ASSERT(resShaderVs > 0, "Failed to create vertex shader");

	GLuint resShaderPs = compileShader(GL_FRAGMENT_SHADER, psText, psSize);
	F_ASSERT(resShaderVs > 0, "Failed to create pixel shader");

	_shader[name] = 0;

	GLuint res = 0;
	if (resShaderPs > 0 && resShaderVs > 0)
	{
		res = glCreateProgram();
		glAttachShader(res, resShaderPs);
		glAttachShader(res, resShaderVs);

		//glBindAttribLocation(res, 0, "inPosition0");
		//glBindAttribLocation(res, 1, "inTexCoord0");
		//glBindAttribLocation(res, 2, "inColor");

		glLinkProgram(res);

		GLint status = 0;
		glGetProgramiv(res, GL_LINK_STATUS, &status);
		F_UNUSED(status);
		F_ASSERT(status == GL_TRUE, "Failed to link program");

		glDetachShader(res, resShaderPs);
		glDetachShader(res, resShaderVs);

		_shader[name] = res;

		// uniforms
		UniformsLocation& uniformsLocation(_shaderUniformsLocation[name]);
		uniformsLocation.tex1 = glGetUniformLocation(_shader[name], "tex1");
		uniformsLocation.screenSize = glGetUniformLocation(_shader[name], "screenSize");
		uniformsLocation.scissorBuffers = glGetUniformLocation(_shader[name], "scissorBuffers");

		return true;
	}

	return false;
}

GLuint fcl::Renderer::compileShader(GLenum shaderType, const char* text, int size)
{
	GLuint resShader = glCreateShader(shaderType);
	glShaderSource(resShader, 1, (GLchar**)(&text), static_cast<GLint*>(&size));
	glCompileShader(resShader);

	GLint status = 0;
	glGetShaderiv(resShader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		GLsizei outputLength = 0;
		GLint logLength = 0;
		glGetShaderiv(resShader, GL_INFO_LOG_LENGTH, &logLength);

		char* buffer = static_cast<char*>(malloc(sizeof(char) * logLength));
		glGetShaderInfoLog(resShader, logLength, &outputLength, buffer);

		F_ASSERT(0, "Failed to compile shader:\n%s", buffer); // in buffer

		free(buffer);

		glDeleteShader(resShader);

		return 0;
	}

	return resShader;
}

void fcl::Renderer::update()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// controls
	setBufferData(GL_ARRAY_BUFFER, Buffers::BUFFERS_UI_VB, &_uiBuffer);
	setVertexLayout(VertexLayouts::V2C);
	bindShader(Shaders::SHADER_COLOR);
	setBlendMode(BlendMode::ALPHA);
	glDrawArrays(GL_TRIANGLES, 0, _uiBuffer.getUsedLength());

	// text
	setBufferData(GL_ARRAY_BUFFER, Buffers::BUFFERS_TEXT_VB, &_textBuffer);
	setVertexLayout(VertexLayouts::V2TC);
	bindShader(Shaders::SHADER_TEXTURE_AND_COLOR);
	setBlendMode(BlendMode::ALPHA);
	setTexture(GL_TEXTURE0, _fontTexID);
	glDrawArrays(GL_TRIANGLES, 0, _textBuffer.getUsedLength());

	/* reset */
	_textBuffer.reset();
	_uiBuffer.reset();
	_scissorLast = 1;
}

void fcl::Renderer::onSurfaceResize(int width, int height)
{
	glViewport(0, 0, width, height);
	_surfaceSize = fsh::float2(width, height);
}

void fcl::Renderer::setVertexLayout(const VertexLayouts& layout)
{
	if (_currentLayout != layout)
	{
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);

		switch (layout)
		{
			case VertexLayouts::NONE:
				// already disabled
				break;

			case VertexLayouts::V2TC:
				glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, fsh::Vec2TC::stride, OFFSETOF(fsh::Vec2TC, pos));
				glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, fsh::Vec2TC::stride, OFFSETOF(fsh::Vec2TC, tex));
				glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, fsh::Vec2TC::stride, OFFSETOF(fsh::Vec2TC, color));
				glVertexAttribIPointer(3, 1, GL_INT, fsh::Vec2TC::stride, OFFSETOF(fsh::Vec2TC, scissorId));
				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(1);
				glEnableVertexAttribArray(2);
				glEnableVertexAttribArray(3);
				break;

			case VertexLayouts::V2C:
				glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, fsh::Vec2C::stride, OFFSETOF(fsh::Vec2C, pos));
				glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, fsh::Vec2C::stride, OFFSETOF(fsh::Vec2C, color));
				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(2);
				break;
		}

		_currentLayout = layout;
	}
}

void fcl::Renderer::setBlendMode(const BlendMode& mode)
{
	if (_currentBlendMode != mode)
	{
		switch (mode)
		{
		case BlendMode::ALPHA:
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;

		case BlendMode::DISABLED:
			glDisable(GL_BLEND);
			break;
		}

		_currentBlendMode = mode;
	}
}

void fcl::Renderer::bindBuffer(GLenum type, const Buffers& buffer)
{
	F_ASSERT(buffer != Buffers::BUFFERS_MAX, "Unsupported buffer");

	if (buffer != Buffers::BUFFERS_MAX && _currentBuffer != buffer)
	{
		if (buffer == Buffers::BUFFERS_NONE)
			glBindBuffer(type, 0);
		else
			glBindBuffer(type, _buffers[buffer]);

		_currentBuffer = buffer;
	}
}

void fcl::Renderer::setBufferData(GLenum type, const Buffers& buffer, IVertexData* data)
{
	F_ASSERT(buffer != Buffers::BUFFERS_NONE, "Wrong buffer");

	if (buffer != Buffers::BUFFERS_NONE)
	{
		bindBuffer(type, buffer);

		void* dst = glMapBufferRange(GL_ARRAY_BUFFER, 0, data->getSize(), GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT | GL_MAP_INVALIDATE_RANGE_BIT);
		F_ASSERT(dst, "Failed to map buffer");
		if (dst)
		{
			memcpy(dst, data->getData(), data->getSize());
			glUnmapBuffer(GL_ARRAY_BUFFER);
		}
	}
}

void fcl::Renderer::bindShader(const Shaders& name)
{
	F_ASSERT(name != Shaders::SHADER_MAX, "Wrong shader");

	if (name != Shaders::SHADER_MAX && _currentShader != name)
	{
		if (name == Shaders::SHADER_NONE)
		{
			glUseProgram(0);
		}
		else
		{
			glUseProgram(_shader[name]);

			// update surface size
			GLint loc = _shaderUniformsLocation[name].screenSize;

			if (loc >= 0)
				glUniform2fv(loc, 1, static_cast<GLfloat*>(&_surfaceSize.x));

			// scissors buffers
			loc = _shaderUniformsLocation[name].scissorBuffers;
			if (loc >= 0)
			{
				_scissorBuffers[0] = fsh::float4(0, 0, _surfaceSize.x, _surfaceSize.y);
				//for (int i(0); i < SCISSORS_MAX; ++i)
				//	_scissorBuffers[i] = fsh::float4(i, i, i, i);

				glUniform4fv(loc, SCISSORS_MAX, static_cast<GLfloat*>(&_scissorBuffers[0].x));
			}
		}

		_currentShader = name;
	}
}

void fcl::Renderer::setTexture(GLenum unit, GLuint texture)
{
	if (_currentShader != Shaders::SHADER_NONE)
	{
		GLint loc = _shaderUniformsLocation[_currentShader].tex1;

		if (loc >= 0)
		{
			glEnable(GL_TEXTURE_2D);
			glActiveTexture(unit);
			glBindTexture(GL_TEXTURE_2D, texture);
			glUniform1i(loc, 0);
		}
	}
}
