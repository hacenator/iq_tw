#pragma once

#include <vector>
#include <common.h>
#include <types.h>
#include <functional>
#include "layout_manager.h"
#include "renderer.h"

namespace fcl
{

namespace ui
{

enum class Type
{
	TEXT,
	PANEL,
	BUTTON,
	TEXTAREA,
};

/* Gravity options */
enum GravityParam
{
	// horizontal
	GRAVITY_LEFT = 2,
	GRAVITY_RIGHT = 4,
	GRAVITY_CENTERH = 8,
	GRAVITY_ABSOLUTEH = 16,

	// vertical
	GRAVITY_TOP = 32,
	GRAVITY_BOTTOM = 64,
	GRAVITY_CENTERV = 128,
	GRAVITY_ABSOLUTEV = 256,
};

/* Fill */
enum FillParam
{
	FILL_MATCH_PARENT = -1,
};

class LayoutControl;

/* control */
class Control
{
friend class LayoutControl;
public:
	Control() 
		: _parent(nullptr)
		, _gravity(GRAVITY_LEFT | GRAVITY_TOP)
	{

	}

	~Control()
	{

	}

	virtual void update();
	virtual void draw(const fsh::float4* const scissor = nullptr) { }

	// input events
	virtual void onMouseMove(int x, int y) { }
	virtual void onMouseOver() { }
	virtual void onMouseOut() { }
	virtual void onClick(int x, int y) { }
	virtual void onText(const wchar_t symbol) { }
	virtual void onKey(int scancode, KeyAction action) { }

	// properties
	virtual void setPosDp(int x, int y)
	{
		_posDp = fsh::float2(x, y);
		if (_parent)
			update();
	}

	virtual void setSizeDp(int x, int y)
	{
		_sizeDp = fsh::float2(x, y);
		if (_parent)
			update();
	}

	virtual void setPaddingDp(int top, int right, int bottom, int left)
	{
		_paddingDp = fsh::float4(top, right, bottom, left);
		if (_parent)
			update();
	}

	virtual void setGravity(int gravity)
	{
		_gravity = gravity;
		if (_parent)
			update();
	}

	const fsh::float2& getPosPx() const
	{
		return _posPx;
	}

	const fsh::float2& getSizePx() const
	{
		return _sizePx;
	}

	bool isFocused() const
	{
		return _isFocused;
	}

	// helpers
	bool hover(const fsh::float2& p);

protected:
	LayoutControl* _parent;

	fsh::float2 _posDp;
	fsh::float2 _sizeDp;
	fsh::float4 _paddingDp;

	fsh::float2 _posPx;
	fsh::float2 _sizePx;

	bool _isFocused;

	int _gravity;
};

// layout
class LayoutControl
	: public Control
{
public:
	LayoutControl()
		: _density(1.0f)
		, _controlFocused(nullptr)
		, _controlHover(nullptr)
	{
	}

	void update() override;
	void draw(const fsh::float4* const scissor = nullptr) override;

	// input events
	virtual void onMouseMove(int x, int y) override;
	virtual void onMouseOver() override;
	virtual void onMouseOut() override;
	virtual void onClick(int x, int y) override;
	virtual void onText(const wchar_t symbol) override;
	virtual void onKey(int scancode, KeyAction action) override;

	// controls managment
	void addControl(Control* control)
	{
		control->_parent = this;
		control->update();

		_controls.push_back(control);
	}

	// properties
	virtual void setSizePx(int x, int y);
	
	float getDensity() const
	{
		return _density;
	}

	fsh::float2 getSizeDp() const
	{

		return fsh::float2(_sizePx.x / _density, _sizePx.y / _density);
	}

protected:
	std::vector<Control*> _controls;

	float _density;

	//
	Control* _controlFocused;
	Control* _controlHover;
};

// text
class TextControl 
	: public Control
{
public:
	TextControl()
	{
		_sizeDp = fsh::float2(0, 32);
	}

	void draw(const fsh::float4* const scissor = nullptr) override;

	// properties
	void setText(const std::wstring& text)
	{
		_text = text;
	}

	const std::wstring& getText() const
	{
		return _text;
	}

	void setTextColor(const fsh::float4& textColor)
	{
		_textColor = textColor;
	}

protected:
	std::wstring _text;
	fsh::float4 _textColor;
};

// panel
class PanelControl
	: public LayoutControl
{
public:
	void update() override;
	void draw(const fsh::float4* const scissor = nullptr) override;

	void setBackColor(const fsh::float4& backColor)
	{
		_backColor = backColor;
	}

protected:
	fsh::float4 _backColor;
};

// button
class ButtonControl
	: public PanelControl
{
public:
	ButtonControl()
	{
		addControl(&_text);
		_text.setGravity(GRAVITY_CENTERH | GRAVITY_CENTERV);
	}

	void draw(const fsh::float4* const scissor = nullptr) override;

	// events
	void onMouseMove(int x, int y) override;
	void onMouseOver() override;
	void onMouseOut() override;
	void onClick(int x, int y) override;

	// properties
	void setText(const std::wstring& text)
	{
		_text.setText(text);
	}

	void setTextColor(const fsh::float4& textColor)
	{
		_defaultTextColor = textColor;
		_text.setTextColor(textColor);
	}

	void setHoverTextColor(const fsh::float4& textColor)
	{
		_hoverTextColor = textColor;
	}

	// callbacks
	void setOnClick(std::function<void()> func)
	{
		_onClick = func;
	}

protected:
	std::function<void()> _onClick;
	
	// text
	TextControl _text;
	fsh::float4 _defaultTextColor;
	fsh::float4 _hoverTextColor;
};

// textarea
class TextareaControl
	: public PanelControl
{
public:
	TextareaControl();

	void draw(const fsh::float4* const scissor = nullptr) override;

	// events
	void onMouseOver() override;
	void onMouseOut() override;
	void onClick(int x, int y) override;
	void onText(const wchar_t symbol) override;
	void onKey(int scancode, KeyAction action) override;

	// properties
	void setText(const std::wstring& text);

	std::wstring getText() const
	{
		if (_placeholder)
			return L"";
		else
			return _text.getText();
	}

	void setPlaceholderText(const std::wstring& text)
	{
		_placeholderText = text;
		if (!_placeholder && _text.getText().length() == 0)
		{
			_placeholder = true;
			_text.setText(_placeholderText);
		}
	}

	void setTextColor(const fsh::float4& textColor)
	{
		_defaultTextColor = textColor;
		_text.setTextColor(textColor);
	}

	void setHoverTextColor(const fsh::float4& textColor)
	{
		_hoverTextColor = textColor;
	}

	// callbacks
	void setOnKeyPress(std::function<void(int key)> func)
	{
		_onKeyPress = func;
	}

protected:
	std::function<void(int key)> _onKeyPress;

	const int textPaddingX = 5;

	bool _placeholder;
	std::wstring _placeholderText;
	uint16_t _cursor;

	TextControl _text;

	fsh::float4 _defaultTextColor;
	fsh::float4 _hoverTextColor;
};

/* TextList */
class TextList
	: public Control
{
public:
	void draw(const fsh::float4* const scissor = nullptr) override;

	void addLine(const std::wstring& line);

private:
	std::vector<std::wstring> _lines;
};

}
}
