#include "client.h"

#include <join_message.h>
#include <chat_message.h>

#include "join_protocol.h"
#include "chat_protocol.h"


fcl::Client::Client()
    : _clientSocket(nullptr)
	, _isJoined(false)
{

}

fcl::Client::~Client()
{

}

bool fcl::Client::connect(const char* addr, uint16_t port)
{
    bool result = false;
    if (_clientSocket)
    {
        result = _clientSocket->connect(addr, port);
    }
    return result;
}

void fcl::Client::init()
{
    Peer::init();

    _clientSocket = getSocketManager()->createSocket();
    F_ASSERT(_clientSocket, "Failed to initialize socket for client");

    // client protocols
    registerProtocol(new fcl::ProtocolJoin(this));
	registerProtocol(new fcl::ProrocolChat(this));
}

void fcl::Client::destroy()
{
    getSocketManager()->destroySocket(_clientSocket);
    Peer::destroy();
}

void fcl::Client::onConnected(const fsh::PeerID& peer)
{
    _serverPeer = peer;

    fsh::MessageFactory* msgFactory(getMessageFactory());
    fsh::MessageJoinRequest* msg = static_cast<fsh::MessageJoinRequest*>(msgFactory->allocMessage(fsh::EMessageType::NET_MESSAGE_JOINREQUEST));
    if (msg)
    {
        msg->addDstPeer(peer);
        msg->setVersionCode(F_VERSIONCODE);
        msg->setNickname(L"user");
    }
    registerOutcomingMessage(msg);
}

void fcl::Client::onDisconnected(const fsh::PeerID& peer)
{

}

void fcl::Client::onJoin()
{
	_isJoined = true;

	if (_onJoin)
		_onJoin();
}

void fcl::Client::onLeave()
{
	_isJoined = false;
    _clientSocket->close();

	if (_onLeave)
		_onLeave();
}

void fcl::Client::onMessage(const std::wstring& message)
{
	if (_onMessage)
		_onMessage(message);
}

void fcl::Client::sendText(const wchar_t* text)
{
    if (text)
    {
        const size_t length(wcslen(text));
        if (length)
        {
            fsh::MessageChat* msg = static_cast<fsh::MessageChat*>(getMessageFactory()->allocMessage(fsh::EMessageType::NET_MESSAGE_CHATMESSAGE));
            F_ASSERT(msg, "Failed to allocate message");
            if (msg)
            {
                msg->setText(text);
                msg->addDstPeer(_clientSocket->getId());
                registerOutcomingMessage(msg);
            }
        }
    }
}

