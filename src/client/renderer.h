#pragma once

#include <common.h>
#include <types.h>

#include <singleton.h>
#include "vertex_data.h"

#include <GL/glew.h>
#include <stb/stb_truetype.h>

namespace fcl
{

class Renderer;

class Renderer
	: public fsh::Singleton<Renderer>
{
public:
	Renderer();
	~Renderer();

	void init();
	void destroy();

	void update();

	void onSurfaceResize(int width, int height);

	// 
	void drawRectangle(const fsh::float2& pos, const fsh::float2& size, const fsh::float4& color, float rounded = 0.0f);
	void drawText(const fsh::float2& pos, const wchar_t* text, const fsh::float4& color, const fsh::float4* scissor = nullptr);

	float textWidth(const wchar_t* text, size_t length);

	const fsh::float2& getSurfaceSize() const
	{
		return _surfaceSize;
	}

private:
	// common settings
	/* vertex layout */
	enum class VertexLayouts
	{
		NONE,
		V2TC,
		V2C,
	};
	void setVertexLayout(const VertexLayouts& layout);

	/* blend */
	enum class BlendMode
	{
		NONE,		// default blend mode for current device
		DISABLED,
		ALPHA,
	};
	void setBlendMode(const BlendMode& mode);

	/* buffers */
	enum Buffers
	{
		BUFFERS_NONE = -1,

		BUFFERS_TEXT_VB,
		BUFFERS_UI_VB,

		BUFFERS_MAX,
	};
	void bindBuffer(GLenum type, const Buffers& buffer);
	void setBufferData(GLenum type, const Buffers& buffer, IVertexData* data);

	/* shaders */
	enum Shaders
	{
		SHADER_NONE = -1,
		SHADER_TEXTURE_AND_COLOR,
		SHADER_COLOR,

		SHADER_MAX,
	};
	bool createProgram(const Shaders& name, const char* vsText, int vsSize, const char* psText, int psSize);
	GLuint compileShader(GLenum shaderType, const char* text, int size);
	void bindShader(const Shaders& name);
	GLuint _shader[SHADER_MAX];
	struct UniformsLocation
	{
		GLint tex1;
		GLint screenSize;
		GLint scissorBuffers;
	};
	UniformsLocation _shaderUniformsLocation[SHADER_MAX];

	/* textures */
	void setTexture(GLenum unit, GLuint texture);

	GLuint _defaultVAO;
	GLint _defaultShaderTexureLocation;

	VertexLayouts _currentLayout;
	BlendMode _currentBlendMode;
	GLint _currentBuffer;
	GLint _currentShader;

	// font
	GLuint _fontTexID;
	stbtt_bakedchar _fontData[96];

	// Data buffers
	VertexData<fsh::Vec2TC> _textBuffer;
	VertexData<fsh::Vec2C> _uiBuffer;

	// OpenGL buffers
	GLuint _buffers[Buffers::BUFFERS_MAX];

	// indexed scissors
	enum Scissors
	{
		SCISSORS_UNDEFINED = 0,
		SCISSORS_MAX = 16
	};
	uint32_t _scissorLast;
	fsh::float4 _scissorBuffers[SCISSORS_MAX];
	uint32_t addScissor(const fsh::float4& scissor);

	fsh::float2 _surfaceSize;
};

}
