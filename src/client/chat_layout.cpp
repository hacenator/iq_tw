#include "chat_layout.h"

#include "client.h"
#include <GLFW/glfw3.h>

/*
style
*/
namespace style
{
	static const fsh::float4 headerBackColor(fsh::rgbaToFloat4(86, 130, 163));
	static const fsh::float4 headerFontColor(fsh::rgbaToFloat4(255, 255, 255));

	static const fsh::float4 footerBackColor(fsh::rgbaToFloat4(255, 255, 255));

	static const fsh::float4 messageBackColor(fsh::rgbaToFloat4(228, 236, 242));
	static const fsh::float4 messageFontColor(fsh::rgbaToFloat4(0, 0, 0));

	static const fsh::float4 messageInBackColor(fsh::rgbaToFloat4(241, 241, 241));
	static const fsh::float4 messageInFontColor(fsh::rgbaToFloat4(0, 0, 0));

	static const fsh::float4 buttonBackColor(fsh::rgbaToFloat4(255, 255, 255));
	static const fsh::float4 buttonFontColor(fsh::rgbaToFloat4(73, 157, 217));
	static const fsh::float4 buttonHoverFontColor(fsh::rgbaToFloat4(43, 127, 187));

	static const fsh::float4 textareaBackColor(fsh::rgbaToFloat4(217, 219, 222));
	static const fsh::float4 textareaFontColor(fsh::rgbaToFloat4(153, 153, 153));
	static const fsh::float4 textareaHoverFontColor(fsh::rgbaToFloat4(123, 123, 123));
}

fcl::ChatLayout::ChatLayout(Renderer* renderer)
	: Layout(renderer)
{
	// relations
	_lSurface.addControl(&_pHeader);
	_lSurface.addControl(&_tlMessages);
	_lSurface.addControl(&_pFooter);
	_pHeader.addControl(&_tHeader);
	_pFooter.addControl(&_iMessage);
	_pFooter.addControl(&_bSend);

	/* header */
	// panel
	_pHeader.setSizeDp(fcl::ui::FILL_MATCH_PARENT, 50);
	_pHeader.setGravity(fcl::ui::GRAVITY_LEFT | fcl::ui::GRAVITY_TOP);
	_pHeader.setBackColor(style::headerBackColor);
	// text
	_tHeader.setText(L"Active Chat");
	_tHeader.setTextColor(style::headerFontColor);
	_tHeader.setGravity(fcl::ui::GRAVITY_LEFT | fcl::ui::GRAVITY_CENTERV);
	_tHeader.setPaddingDp(0, 0, 0, 10);

	/* body */
	_tlMessages.setGravity(fcl::ui::GRAVITY_LEFT | fcl::ui::GRAVITY_ABSOLUTEV);
	_tlMessages.setPosDp(0, 50);
	_tlMessages.setPaddingDp(0, 0, 20, 20);

	/* footer */
	// panel
	_pFooter.setSizeDp(fcl::ui::FILL_MATCH_PARENT, 50);
	_pFooter.setGravity(fcl::ui::GRAVITY_LEFT | fcl::ui::GRAVITY_BOTTOM);
	_pFooter.setBackColor(style::footerBackColor);
	// textarea
	_iMessage.setPlaceholderText(L"Enter text here...");
	_iMessage.setBackColor(style::textareaBackColor);
	_iMessage.setTextColor(style::textareaFontColor);
	_iMessage.setHoverTextColor(style::textareaHoverFontColor);
	_iMessage.setGravity(fcl::ui::GRAVITY_LEFT | fcl::ui::GRAVITY_CENTERV);
	_iMessage.setSizeDp(400, 40);
	_iMessage.setPaddingDp(0, 0, 0, 10);
	_iMessage.setOnKeyPress([this](int key)
	{
		if (key == GLFW_KEY_ENTER)
			this->send();
	});
	// button
	_bSend.setText(L"SEND");
	_bSend.setBackColor(style::buttonBackColor);
	_bSend.setTextColor(style::buttonFontColor);
	_bSend.setHoverTextColor(style::buttonHoverFontColor);
	_bSend.setSizeDp(200, 40);
	_bSend.setGravity(fcl::ui::GRAVITY_RIGHT | fcl::ui::GRAVITY_CENTERV);
	_bSend.setOnClick([this]()
	{
		this->send();
	});
}

fcl::ChatLayout::~ChatLayout()
{

}

void fcl::ChatLayout::addMessage(const std::wstring& msg)
{
	_tlMessages.addLine(msg.c_str());
}

void fcl::ChatLayout::send()
{
	fcl::Client* client(fcl::Client::getInstance());

	std::wstring text = _iMessage.getText();
	if (text.length())
	{
		client->sendText(text.c_str());
		addMessage(text);

		_iMessage.setText(L"");
	}
}

void fcl::ChatLayout::onSurfaceResize(int width, int height)
{
	const fsh::float2 surfaceSize(_renderer->getSurfaceSize());

	_lSurface.setSizePx(surfaceSize.x, surfaceSize.y);
	_tlMessages.setSizeDp(fcl::ui::FILL_MATCH_PARENT, _lSurface.getSizeDp().y - 100);
}

void fcl::ChatLayout::onMouseMove(int x, int y)
{
	_lSurface.onMouseMove(x, y);
}

void fcl::ChatLayout::onClick(int x, int y)
{
	_lSurface.onClick(x, y);
}

void fcl::ChatLayout::onText(const wchar_t symbol)
{
	_lSurface.onText(symbol);
}

void fcl::ChatLayout::onKey(int scancode, KeyAction action)
{
	_lSurface.onKey(scancode, action);
}

void fcl::ChatLayout::draw()
{
	_lSurface.draw();
}
