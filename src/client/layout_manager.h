#pragma once

#include <singleton.h>

namespace fcl
{

class Renderer;

/* Key action */
enum class KeyAction
{
	RELEASE = 0,
	PRESS = 1,
	REPEAT = 2,
};

class Layout
{
public:
	Layout(Renderer* renderer)
		: _renderer(renderer)
	{
	}

	virtual ~Layout()
	{
	}

	virtual void onSurfaceResize(int width, int height) = 0;
	virtual void onMouseMove(int x, int y) = 0;
	virtual void onClick(int x, int y) = 0;

	virtual void onText(const wchar_t symbol) = 0;
	virtual void onKey(int scancode, KeyAction action) = 0;

	virtual void onShow() { }
	virtual void onHide() { }

	virtual void draw() = 0;

protected:
	Renderer* _renderer;
};

class LayoutManager :
	public fsh::Singleton<LayoutManager>
{
public:
	LayoutManager();
	~LayoutManager();

	void onSurfaceResize(int width, int height);
	void onMouseMove(int x, int y);
	void onClick();
	void onText(const wchar_t symbol);
	void onKey(int scancode, KeyAction action);

	void draw();

	enum ELayout
	{
		LAYOUTS_NONE = -1,

		LAYOUTS_CONNECTION,
		LAYOUTS_CHAT,
		
		LAYOUTS_MAX
	};
	void enableLayout(ELayout layout);

	Layout* getLayout(ELayout layout) const
	{
		return _layouts[layout];
	}

private:
	void setLayout(Layout* stage);

	Layout* _layouts[LAYOUTS_MAX];

	int _surfaceWidth;
	int _surfaceHeight;

	int _pointerX;
	int _pointerY;

	Layout* _currentLayout;
};

}
