#pragma once

#include <protocol.h>

namespace fcl
{

class ProrocolChat :
    public fsh::Protocol
{
public:
    explicit ProrocolChat(fsh::Peer* handler);
    ~ProrocolChat();

    fsh::EProtocolResult processMessage(const fsh::PeerID& peer, const fsh::Message* msg) override;
};

}
