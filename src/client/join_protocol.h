#pragma once

#include <common.h>
#include <protocol.h>

namespace fcl
{

class ProtocolJoin final
	: public fsh::Protocol
{
public:
	explicit ProtocolJoin(fsh::Peer* handler);
	~ProtocolJoin();

	fsh::EProtocolResult processMessage(const fsh::PeerID& peer, const fsh::Message* msg) override;
};

}
